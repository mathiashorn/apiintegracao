using System;    
using System.Collections.Generic;
using System.Collections.ObjectModel;    
using System.Data;    
//using System.Data.SqlClient;  
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;  
using System.Threading;


public class BolsaFamilia
{

    public static int states = 0;
    private static IWebDriver driver = new ChromeDriver("./");
    
    public static void Main(string[] args) {
    
        for (int i = 0; i < 765; i++)
        {

            driver.Navigate().GoToUrl("https://www.lajeado.rs.gov.br/?titulo=Portal%20da%20Transpar%EAncia&template=servico&categoria=972&codigoCategoria=972&servico=70468&idConteudo=3542");

            Thread.Sleep(4000);

            driver.SwitchTo().Frame(driver.FindElement(By.Id("frameServicos")));            
            IWebElement elementBt = driver.FindElement(By.Id("form:j_id_3o:0:j_id_3p"));
            elementBt.Click();

            Thread.Sleep(3500);

            IWebElement elementTr = driver.FindElement(By.Id("form:dataTableConvenios:dataTable:" + (i) + ":j_id_bz:j_id_c0"));
            elementTr.Click();

            Thread.Sleep(2000);

            String campoExercicio = driver.FindElement(By.Id("form:campoExercicio")).GetAttribute("value");
            String campoAdministracao = driver.FindElement(By.Id("form:campoAdministracao:field")).GetAttribute("value");
            String campoSituacao = driver.FindElement(By.Id("form:campoSituacao:field")).GetAttribute("value");
            if (campoSituacao == "Ativo")
            {
                campoSituacao = "A";
            } else {
                campoSituacao = "I";
            }
            String campoConvenenteAba = driver.FindElement(By.Id("form:campoConvenenteAba:field")).GetAttribute("value");
            String campoConcedenteAba = driver.FindElement(By.Id("form:campoConcedenteAba:field")).GetAttribute("value");
            String campoIntervenienteAba = driver.FindElement(By.Id("form:campoIntervenienteAba:field")).GetAttribute("value");
            String campoTipoAba = driver.FindElement(By.Id("form:campoTipoAba:field")).GetAttribute("value");
            String campoClassificacaoAba = driver.FindElement(By.Id("form:campoClassificacaoAba:field")).GetAttribute("value");
            String campoObservacaoAba = driver.FindElement(By.Id("form:campoObservacaoAba")).GetAttribute("value");
            String campoDataAssinaturaAba = driver.FindElement(By.Id("form:campoDataAssinaturaAba:fieldDate:field")).GetAttribute("value");
            String campoDataEncerramentoAba = driver.FindElement(By.Id("form:campoDataEncerramentoAba:fieldDate:field")).GetAttribute("value");
            String campoDataRescisaoAba = driver.FindElement(By.Id("form:campoDataRescisaoAba:fieldDate:field")).GetAttribute("value");
            String campoDataInicioAba = driver.FindElement(By.Id("form:campoDataInicioAba:fieldDate:field")).GetAttribute("value");
            String campoDataVenctoAba = driver.FindElement(By.Id("form:campoDataVenctoAba:fieldDate:field")).GetAttribute("value");
            String campoDataVenctoOriginalAba = driver.FindElement(By.Id("form:campoDataVenctoOriginalAba:fieldDate:field")).GetAttribute("value");
            String campoRecursoAba = driver.FindElement(By.Id("form:campoRecursoAba:field")).GetAttribute("value");
            String campoRrecursoContrapartidaAba = driver.FindElement(By.Id("form:campoRrecursoContrapartidaAba:field")).GetAttribute("value");
            String campoValorRrecursoAba = driver.FindElement(By.Id("form:campoValorRrecursoAba:fieldNumber:field")).GetAttribute("value");
            String campoValorContrapartidaAba = driver.FindElement(By.Id("form:campoValorContrapartidaAba:fieldNumber:field")).GetAttribute("value");
            String campoValorConvenioOriginalAba = driver.FindElement(By.Id("form:campoValorConvenioOriginalAba:fieldNumber:field")).GetAttribute("value");
            String campoValorAba = driver.FindElement(By.Id("form:campoValorAba:fieldNumber:field")).GetAttribute("value");
            
            Console.Write(campoExercicio.Replace(";", "") + ";");
            Console.Write(campoAdministracao.Replace(";", "") + ";");
            Console.Write(campoSituacao.Replace(";", "") + ";");
            Console.Write(campoConvenenteAba.Replace(";", "") + ";");
            Console.Write(campoConcedenteAba.Replace(";", "") + ";");
            Console.Write(campoIntervenienteAba.Replace(";", "") + ";");
            Console.Write(campoTipoAba.Replace(";", "") + ";");
            Console.Write(campoClassificacaoAba.Replace(";", "") + ";");
            Console.Write(campoObservacaoAba.Replace(";", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "") + ";");
            Console.Write(campoDataAssinaturaAba + ";");    
            Console.Write(campoDataEncerramentoAba + ";");
            Console.Write(campoDataRescisaoAba + ";");
            Console.Write(campoDataInicioAba + ";");
            Console.Write(campoDataVenctoAba + ";");
            Console.Write(campoDataVenctoOriginalAba.Replace(";", "") + ";");
            Console.Write(campoRecursoAba.Replace(";", "") + ";");
            Console.Write(campoRrecursoContrapartidaAba.Replace(";", "") + ";");
            Console.Write(campoValorRrecursoAba.Replace(".", "").Replace(",", ".") + ";");
            Console.Write(campoValorContrapartidaAba.Replace(".", "").Replace(",", ".") + ";");
            Console.Write(campoValorConvenioOriginalAba.Replace(".", "").Replace(",", ".") + ";");
            Console.WriteLine(campoValorAba.Replace(".", "").Replace(",", "."));

            Thread.Sleep(3500);
        }


    }

    
}