-- Table: public.convenio

-- DROP TABLE public.convenio;

CREATE SEQUENCE convenio_seq;

CREATE TABLE public.convenio
(
  id integer NOT NULL DEFAULT nextval('convenio_seq'::regclass),
  convenio character varying(11) NULL,
  administracao character varying(50) NULL,
  situacao character(1) NULL,
  convenente character varying(100) NULL,
  concedente character varying(100) NULL,
  interveniente character varying(100) NULL,
  tipo character varying(50) NULL,
  classificacao character varying(50) NULL,
  objeto text NULL,
  data_assinatura timestamp(0) without time zone,
  data_encerramento timestamp(0) without time zone,
  data_rescisao timestamp(0) without time zone,
  data_inicio timestamp(0) without time zone,
  data_vencimento timestamp(0) without time zone,
  data_vencimento_original timestamp(0) without time zone,
  recurso character varying(100) NULL,
  contrapartida character varying(50) NULL,
  valor_recurso decimal(12,2) NULL,
  valor_contrapartida decimal(12,2) NULL,
  valor_original decimal(12,2) NULL,
  valor_total decimal(12,2) NULL,
  CONSTRAINT convenio_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.convenio
  OWNER TO postgres;
  
INSERT INTO public.convenio
(
	convenio, 
	administracao, 
	situacao, 
	convenente, 
	concedente, 
	interveniente, 
	tipo, 
	classificacao, 
	objeto, 
	data_assinatura, 
	data_encerramento, 
	data_rescisao,
	data_inicio,
	data_vencimento,
	data_vencimento_original,
	recurso,
	contrapartida,
	valor_recurso,
	valor_contrapartida,
	valor_original,
	valor_total
) VALUES (
	'2007/233607',
	'Prefeitura Municipal de Lajeado',
	'A',
	'MUNICIPIO DE LAJEADO (141780)	',
	'MINISTERIO DAS CIDADES', 
	NULL, 
	'Convênio - Anterior a Lei 13.019', 
	'Outros', 
	'CONSTRUÇÃO DE 30 UNIDADES HABITACIONAIS, NO BAIRRO SANTO ANTÔNIO.', 
	'31/12/2007', 
	NULL, 
	NULL,
	'31/12/2007',
	'30/05/2015',
	'30/12/2010',
	'[1006] FMH-Fun.M.Hab BB 52.169-8, CEF 4027-7',
	'[0001] Recurso Livre',
	491400.00,
	367848.05,
	594900.00,
	859248.05
)

-- Table: public.funcionalismo

-- DROP TABLE public.funcionalismo;

CREATE SEQUENCE funcionalismo_seq;

CREATE TABLE public.funcionalismo
(
  id integer NOT NULL DEFAULT nextval('funcionalismo_seq'::regclass),
  matricula integer NULL,
  nome character varying(100) NULL,
  codigo_cargo integer NULL,
  nome_cargo character varying(50) NULL,
  cpf character varying(14) NULL,
  codigo_tipo_mov integer NULL,
  desc_tipo_mov character varying(50) NULL,
  horas_mensais integer NULL,
  data_admissao timestamp(0) without time zone,
  codigo_setor integer NULL,
  nome_setor character varying(50) NULL,
  codigo_divisao integer NULL,
  nome_divisao character varying(50) NULL,
  codigo_orgao integer NULL,
  nome_orgao character varying(50) NULL,
  codigo_centro_custo integer NULL,
  nome_centro_custo character varying(50) NULL,
  codigo_vinculo integer NULL,
  nome_vinculo character varying(50) NULL,
  padrao character varying(50) NULL,
  data_referencia timestamp(0) without time zone,
  inativo character(1) NULL,
  pensionista character(1) NULL,
  tempo_servico integer NULL,
  valor_remuneracao_basica decimal(12,2) NULL,
  valor_verbas_eventuais decimal(12,2) NULL,
  valor_verbas_indenizatorias decimal(12,2) NULL,
  valor_ferias decimal(12,2) NULL,
  valor_decimo_terceiro decimal(12,2) NULL,
  valor_deducoes_obrigatorias decimal(12,2) NULL,
  valor_remuneracao decimal(12,2) NULL,
  CONSTRAINT servidor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.funcionalismo
  OWNER TO postgres;




