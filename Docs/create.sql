-- Table: public.tipo_conexao

-- DROP TABLE public.tipo_conexao;

CREATE SEQUENCE tipo_conexao_seq;

CREATE TABLE public.tipo_conexao
(
  id integer NOT NULL DEFAULT nextval('tipo_conexao_seq'::regclass),
  descricao character varying(45) NOT NULL,
  CONSTRAINT tipo_conexao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tipo_conexao
  OWNER TO postgres;

INSERT INTO public.tipo_conexao(descricao) VALUES ('PostgreSQL');
INSERT INTO public.tipo_conexao(descricao) VALUES ('SQL Server');
  
-- Table: public.usuario

-- DROP TABLE public.usuario;

CREATE SEQUENCE usuario_seq;

CREATE TABLE public.usuario
(
  id integer NOT NULL DEFAULT nextval('usuario_seq'::regclass),
  nome character varying(100) NOT NULL,
  senha character varying(200) NOT NULL,
  email character varying(100) NOT NULL,
  CONSTRAINT usuario_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuario
  OWNER TO postgres; 
  
INSERT INTO public.usuario(nome, senha, email) VALUES ('MATHIAS HORN', 'KOGnHXBha+a53ssT7aMZFb6CYcJVJkNPmWgRqkWURq8=', 'mathias.horn@gmail.com');
  
-- Table: public.conexao

-- DROP TABLE public.conexao;

CREATE SEQUENCE conexao_seq;

CREATE TABLE public.conexao
(
  id integer NOT NULL DEFAULT nextval('conexao_seq'::regclass),
  tipo_conexao_id integer NOT NULL,
  descricao character varying(50) NOT NULL,
  connection_string character varying(200) NOT NULL,
  CONSTRAINT conexao_pkey PRIMARY KEY (id),
  CONSTRAINT fk_conexao_tipo_conexao FOREIGN KEY (tipo_conexao_id)
      REFERENCES public.tipo_conexao (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.conexao
  OWNER TO postgres;

-- Index: public.fk_conexao_tipo_conexao_idx

-- DROP INDEX public.fk_conexao_tipo_conexao_idx;

CREATE INDEX fk_conexao_tipo_conexao_idx
  ON public.conexao
  USING btree
  (tipo_conexao_id);
  
-- Table: public.rotina

-- DROP TABLE public.rotina;

CREATE SEQUENCE rotina_seq;

CREATE TABLE public.rotina
(
  id integer NOT NULL DEFAULT nextval('rotina_seq'::regclass),
  conexao_id integer NOT NULL,
  descricao character varying(50) NOT NULL,
  consulta text NOT NULL,
  url_destino character varying(100) NOT NULL,
  CONSTRAINT rotina_pkey PRIMARY KEY (id),
  CONSTRAINT fk_rotina_conexao1 FOREIGN KEY (conexao_id)
      REFERENCES public.conexao (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.rotina
  OWNER TO postgres;

-- Index: public.fk_rotina_conexao1_idx

-- DROP INDEX public.fk_rotina_conexao1_idx;

CREATE INDEX fk_rotina_conexao1_idx
  ON public.rotina
  USING btree
  (conexao_id);
  
-- Table: public.auditoria

-- DROP TABLE public.auditoria;

CREATE SEQUENCE auditoria_seq;

CREATE TABLE public.auditoria
(
  id integer NOT NULL DEFAULT nextval('auditoria_seq'::regclass),
  usuario_id integer NOT NULL,
  tabela character varying(50) NOT NULL,
  chave character varying(50) NOT NULL,
  acao character varying(10) NOT NULL,
  data_hora character varying(45) NOT NULL,
  conteudo text,
  CONSTRAINT auditoria_pkey PRIMARY KEY (id),
  CONSTRAINT fk_auditoria_usuario1 FOREIGN KEY (usuario_id)
      REFERENCES public.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auditoria
  OWNER TO postgres;

-- Index: public.fk_auditoria_usuario1_idx

-- DROP INDEX public.fk_auditoria_usuario1_idx;

CREATE INDEX fk_auditoria_usuario1_idx
  ON public.auditoria
  USING btree
  (usuario_id);
  
-- Table: public.execucao

-- DROP TABLE public.execucao;

CREATE SEQUENCE execucao_seq;

CREATE TABLE public.execucao
(
  id integer NOT NULL DEFAULT nextval('execucao_seq'::regclass),
  tipo character(1) NOT NULL,
  rotina_id integer,
  arquivo character varying(200),
  data_hora_inicio timestamp(0) without time zone,
  data_hora_termino timestamp(0) without time zone,
  status character varying(20),
  mensagem_retorno text,
  CONSTRAINT execucao_pkey PRIMARY KEY (id),
  CONSTRAINT fk_execucao_rotina1 FOREIGN KEY (rotina_id)
      REFERENCES public.rotina (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.execucao
  OWNER TO postgres;

-- Index: public.fk_execucao_rotina1_idx

-- DROP INDEX public.fk_execucao_rotina1_idx;

CREATE INDEX fk_execucao_rotina1_idx
  ON public.execucao
  USING btree
  (rotina_id);

-- Table: public.parametro

-- DROP TABLE public.parametro;

CREATE SEQUENCE parametro_seq;

CREATE TABLE public.parametro
(
  id integer NOT NULL DEFAULT nextval('parametro_seq'::regclass),
  chave character varying(100) NOT NULL,
  valor character varying(100) NOT NULL,
  descricao character varying(100),
  CONSTRAINT parametro_pkey PRIMARY KEY (id),
  CONSTRAINT chave_unique UNIQUE (chave)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.parametro
  OWNER TO postgres;

INSERT INTO public.parametro(chave, valor, descricao) VALUES ('enderecoPortalTransparencia', 'http://localhost:64640/api/', 'Endereço do Portal de Transparência');
INSERT INTO public.parametro(chave, valor, descricao) VALUES ('usuarioPortalTransparencia', 'mathias.horn@gmail.com', 'Usuário do Portal de Transparência');
INSERT INTO public.parametro(chave, valor, descricao) VALUES ('senhaPortalTransparencia', 'abc123$', 'Senha do Portal de Transparência');

-- Table: public.permissao

-- DROP TABLE public.permissao;

CREATE SEQUENCE permissao_seq;

CREATE TABLE public.permissao
(
  id integer NOT NULL DEFAULT nextval('permissao_seq'::regclass),
  usuario_id integer NOT NULL,
  descricao character varying(100) NOT NULL,
  permitido boolean NOT NULL,
  CONSTRAINT permissao_pkey PRIMARY KEY (id),
  CONSTRAINT fk_permissoes_usuario1 FOREIGN KEY (usuario_id)
      REFERENCES public.usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.permissao
  OWNER TO postgres;

-- Index: public.fk_permissoes_usuario1_idx

-- DROP INDEX public.fk_permissoes_usuario1_idx;

CREATE INDEX fk_permissoes_usuario1_idx
  ON public.permissao
  USING btree
  (usuario_id);

-- Table: public.variavel

-- DROP TABLE public.variavel;

CREATE SEQUENCE variavel_seq;

CREATE TABLE public.variavel
(
  id integer NOT NULL DEFAULT nextval('variavel_seq'::regclass),
  rotina_id integer NOT NULL,
  nome character varying(45) NOT NULL,
  tipo character varying(45) NOT NULL,
  obrigatoria character varying(45) NOT NULL,
  CONSTRAINT variavel_pkey PRIMARY KEY (id),
  CONSTRAINT fk_variavel_rotina1 FOREIGN KEY (rotina_id)
      REFERENCES public.rotina (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.variavel
  OWNER TO postgres;

-- Index: public.fk_variavel_rotina1_idx

-- DROP INDEX public.fk_variavel_rotina1_idx;

CREATE INDEX fk_variavel_rotina1_idx
  ON public.variavel
  USING btree
  (rotina_id);





