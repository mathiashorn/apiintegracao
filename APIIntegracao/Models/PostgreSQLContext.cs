﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIIntegracao.Models
{
    public partial class PostgreSQLContext : DbContext
    {
        public PostgreSQLContext(string connectionString) : base(GetOptions(connectionString))
        {
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            //return SqlServerDbContextOptionsExtensions.use(new DbContextOptionsBuilder(), connectionString).Options;
            return NpgsqlDbContextOptionsExtensions.UseNpgsql(new DbContextOptionsBuilder(), connectionString).Options;
        }

    }
}
