﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Conexao
    {
        public Conexao()
        {
            Rotina = new HashSet<Rotina>();
        }

        public int Id { get; set; }
        public int TipoConexaoId { get; set; }
        public string Descricao { get; set; }
        public string ConnectionString { get; set; }

        public virtual TipoConexao TipoConexao { get; set; }
        [JsonIgnore]
        public virtual ICollection<Rotina> Rotina { get; set; }
    }
}
