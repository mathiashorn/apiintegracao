﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class TipoConexao
    {
        public TipoConexao()
        {
            Conexao = new HashSet<Conexao>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }

        [JsonIgnore]
        public virtual ICollection<Conexao> Conexao { get; set; }
    }
}
