﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace APIIntegracao.Models
{
    public partial class APIIntegracaoContext : DbContext
    {
        public APIIntegracaoContext()
        {
        }

        public APIIntegracaoContext(DbContextOptions<APIIntegracaoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Auditoria> Auditoria { get; set; }
        public virtual DbSet<Conexao> Conexao { get; set; }
        public virtual DbSet<Execucao> Execucao { get; set; }
        public virtual DbSet<Parametro> Parametro { get; set; }
        public virtual DbSet<Permissao> Permissao { get; set; }
        public virtual DbSet<Rotina> Rotina { get; set; }
        public virtual DbSet<TipoConexao> TipoConexao { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Variavel> Variavel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=APIIntegracao;Username=postgres;Password=postgres");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Auditoria>(entity =>
            {
                entity.ToTable("auditoria");

                entity.HasIndex(e => e.UsuarioId)
                    .HasName("fk_auditoria_usuario1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('auditoria_seq'::regclass)");

                entity.Property(e => e.Acao)
                    .IsRequired()
                    .HasColumnName("acao")
                    .HasMaxLength(10);

                entity.Property(e => e.Chave)
                    .IsRequired()
                    .HasColumnName("chave")
                    .HasMaxLength(50);

                entity.Property(e => e.Conteudo).HasColumnName("conteudo");

                entity.Property(e => e.DataHora)
                    .IsRequired()
                    .HasColumnName("data_hora")
                    .HasMaxLength(45);

                entity.Property(e => e.Tabela)
                    .IsRequired()
                    .HasColumnName("tabela")
                    .HasMaxLength(50);

                entity.Property(e => e.UsuarioId).HasColumnName("usuario_id");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Auditoria)
                    .HasForeignKey(d => d.UsuarioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_auditoria_usuario1");
            });

            modelBuilder.Entity<Conexao>(entity =>
            {
                entity.ToTable("conexao");

                entity.HasIndex(e => e.TipoConexaoId)
                    .HasName("fk_conexao_tipo_conexao_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('conexao_seq'::regclass)");

                entity.Property(e => e.ConnectionString)
                    .IsRequired()
                    .HasColumnName("connection_string")
                    .HasMaxLength(200);

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(50);

                entity.Property(e => e.TipoConexaoId).HasColumnName("tipo_conexao_id");

                entity.HasOne(d => d.TipoConexao)
                    .WithMany(p => p.Conexao)
                    .HasForeignKey(d => d.TipoConexaoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_conexao_tipo_conexao");
            });

            modelBuilder.Entity<Execucao>(entity =>
            {
                entity.ToTable("execucao");

                entity.HasIndex(e => e.RotinaId)
                    .HasName("fk_execucao_rotina1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('execucao_seq'::regclass)");

                entity.Property(e => e.Arquivo)
                    .HasColumnName("arquivo")
                    .HasMaxLength(200);

                entity.Property(e => e.DataHoraInicio)
                    .HasColumnName("data_hora_inicio")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataHoraTermino)
                    .HasColumnName("data_hora_termino")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.MensagemRetorno).HasColumnName("mensagem_retorno");

                entity.Property(e => e.RotinaId).HasColumnName("rotina_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(20);

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.HasOne(d => d.Rotina)
                    .WithMany(p => p.Execucao)
                    .HasForeignKey(d => d.RotinaId)
                    .HasConstraintName("fk_execucao_rotina1");
            });

            modelBuilder.Entity<Parametro>(entity =>
            {
                entity.ToTable("parametro");

                entity.HasIndex(e => e.Chave)
                    .HasName("chave_unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('parametro_seq'::regclass)");

                entity.Property(e => e.Chave)
                    .IsRequired()
                    .HasColumnName("chave")
                    .HasMaxLength(100);

                entity.Property(e => e.Descricao)
                    .HasColumnName("descricao")
                    .HasMaxLength(100);

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("valor")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Permissao>(entity =>
            {
                entity.ToTable("permissao");

                entity.HasIndex(e => e.UsuarioId)
                    .HasName("fk_permissoes_usuario1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('permissao_seq'::regclass)");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(100);

                entity.Property(e => e.Permitido).HasColumnName("permitido");

                entity.Property(e => e.UsuarioId).HasColumnName("usuario_id");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Permissao)
                    .HasForeignKey(d => d.UsuarioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_permissoes_usuario1");
            });

            modelBuilder.Entity<Rotina>(entity =>
            {
                entity.ToTable("rotina");

                entity.HasIndex(e => e.ConexaoId)
                    .HasName("fk_rotina_conexao1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('rotina_seq'::regclass)");

                entity.Property(e => e.ConexaoId).HasColumnName("conexao_id");

                entity.Property(e => e.Consulta)
                    .IsRequired()
                    .HasColumnName("consulta");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(50);

                entity.Property(e => e.UrlDestino)
                    .IsRequired()
                    .HasColumnName("url_destino")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Conexao)
                    .WithMany(p => p.Rotina)
                    .HasForeignKey(d => d.ConexaoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_rotina_conexao1");
            });

            modelBuilder.Entity<TipoConexao>(entity =>
            {
                entity.ToTable("tipo_conexao");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('tipo_conexao_seq'::regclass)");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("usuario");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('usuario_seq'::regclass)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(100);

                entity.Property(e => e.Senha)
                    .IsRequired()
                    .HasColumnName("senha")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Variavel>(entity =>
            {
                entity.ToTable("variavel");

                entity.HasIndex(e => e.RotinaId)
                    .HasName("fk_variavel_rotina1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('variavel_seq'::regclass)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(45);

                entity.Property(e => e.Obrigatoria)
                    .IsRequired()
                    .HasColumnName("obrigatoria")
                    .HasMaxLength(45);

                entity.Property(e => e.RotinaId).HasColumnName("rotina_id");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasMaxLength(45);

                entity.HasOne(d => d.Rotina)
                    .WithMany(p => p.Variavel)
                    .HasForeignKey(d => d.RotinaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_variavel_rotina1");
            });

            modelBuilder.HasSequence("auditoria_seq");

            modelBuilder.HasSequence("conexao_seq");

            modelBuilder.HasSequence("execucao_seq");

            modelBuilder.HasSequence("parametro_seq");

            modelBuilder.HasSequence("permissao_seq");

            modelBuilder.HasSequence("rotina_seq");

            modelBuilder.HasSequence("tipo_conexao_seq");

            modelBuilder.HasSequence("usuario_seq");

            modelBuilder.HasSequence("variavel_seq");
        }
    }
}
