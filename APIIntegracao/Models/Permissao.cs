﻿using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Permissao
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public string Descricao { get; set; }
        public bool Permitido { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
