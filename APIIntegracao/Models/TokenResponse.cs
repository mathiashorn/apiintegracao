﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace APIIntegracao.Models
{
    public class TokenResponse
    {
        [Required]
        [JsonProperty("token")]
        public string Token { get; set; }

        [Required]
        [JsonProperty("expires")]
        public string Expires { get; set; }
    }
}
