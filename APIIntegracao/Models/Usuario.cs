﻿using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            Auditoria = new HashSet<Auditoria>();
            Permissao = new HashSet<Permissao>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Auditoria> Auditoria { get; set; }
        public virtual ICollection<Permissao> Permissao { get; set; }
    }
}
