﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Rotina
    {
        public Rotina()
        {
            Execucao = new HashSet<Execucao>();
            Variavel = new HashSet<Variavel>();
        }

        public int Id { get; set; }
        public int ConexaoId { get; set; }
        public string Descricao { get; set; }
        public string Consulta { get; set; }
        public string UrlDestino { get; set; }

        public virtual Conexao Conexao { get; set; }
        [JsonIgnore]
        public virtual ICollection<Execucao> Execucao { get; set; }
        [JsonIgnore]
        public virtual ICollection<Variavel> Variavel { get; set; }
    }
}
