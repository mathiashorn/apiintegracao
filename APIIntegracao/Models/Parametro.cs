﻿using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Parametro
    {
        public int Id { get; set; }
        public string Chave { get; set; }
        public string Valor { get; set; }
        public string Descricao { get; set; }
    }
}
