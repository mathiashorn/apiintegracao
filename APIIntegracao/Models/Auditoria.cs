﻿using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Auditoria
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public string Tabela { get; set; }
        public string Chave { get; set; }
        public string Acao { get; set; }
        public string DataHora { get; set; }
        public string Conteudo { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
