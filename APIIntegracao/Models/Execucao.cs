﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIIntegracao.Models
{
    public partial class Execucao
    {
        public int Id { get; set; }
        public char Tipo { get; set; }
        public int? RotinaId { get; set; }
        public string Arquivo { get; set; }
        [NotMappedAttribute]
        public IFormFile ArquivoFormFile { set; get; }
        public DateTime? DataHoraInicio { get; set; }
        public DateTime? DataHoraTermino { get; set; }
        public string Status { get; set; }
        public string MensagemRetorno { get; set; }

        public virtual Rotina Rotina { get; set; }
    }
}
