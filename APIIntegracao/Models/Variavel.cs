﻿using System;
using System.Collections.Generic;

namespace APIIntegracao.Models
{
    public partial class Variavel
    {
        public int Id { get; set; }
        public int RotinaId { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public string Obrigatoria { get; set; }

        public virtual Rotina Rotina { get; set; }
    }
}
