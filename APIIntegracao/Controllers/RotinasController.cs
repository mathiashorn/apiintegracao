﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;
using APIIntegracao.Services.ImportModels;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RotinasController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public RotinasController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Rotinas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Rotina>>> GetRotina()
        {
            return await _context.Rotina.Include(r => r.Conexao).ToListAsync();
        }

        // GET: api/Rotinas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Rotina>> GetRotina(int id)
        {
            var rotina = await _context.Rotina.FindAsync(id);

            if (rotina == null)
            {
                return NotFound();
            }

            return rotina;
        }

        // PUT: api/Rotinas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRotina(int id, Rotina rotina)
        {
            if (id != rotina.Id)
            {
                return BadRequest();
            }

            _context.Entry(rotina).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RotinaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Rotinas
        [HttpPost]
        public async Task<ActionResult<Rotina>> PostRotina(Rotina rotina)
        {
            _context.Rotina.Add(rotina);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRotina", new { id = rotina.Id }, rotina);
        }

        // DELETE: api/Rotinas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Rotina>> DeleteRotina(int id)
        {
            var rotina = await _context.Rotina.FindAsync(id);
            if (rotina == null)
            {
                return NotFound();
            }

            _context.Rotina.Remove(rotina);
            await _context.SaveChangesAsync();

            return rotina;
        }

        private bool RotinaExists(int id)
        {
            return _context.Rotina.Any(e => e.Id == id);
        }
    }
}
