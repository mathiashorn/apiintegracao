﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PermissoesController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public PermissoesController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Permissoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Permissao>>> GetPermissao()
        {
            return await _context.Permissao.ToListAsync();
        }

        // GET: api/Permissoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Permissao>> GetPermissao(int id)
        {
            var permissao = await _context.Permissao.FindAsync(id);

            if (permissao == null)
            {
                return NotFound();
            }

            return permissao;
        }

        // PUT: api/Permissoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPermissao(int id, Permissao permissao)
        {
            if (id != permissao.Id)
            {
                return BadRequest();
            }

            _context.Entry(permissao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PermissaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Permissoes
        [HttpPost]
        public async Task<ActionResult<Permissao>> PostPermissao(Permissao permissao)
        {
            _context.Permissao.Add(permissao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPermissao", new { id = permissao.Id }, permissao);
        }

        // DELETE: api/Permissoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Permissao>> DeletePermissao(int id)
        {
            var permissao = await _context.Permissao.FindAsync(id);
            if (permissao == null)
            {
                return NotFound();
            }

            _context.Permissao.Remove(permissao);
            await _context.SaveChangesAsync();

            return permissao;
        }

        private bool PermissaoExists(int id)
        {
            return _context.Permissao.Any(e => e.Id == id);
        }
    }
}
