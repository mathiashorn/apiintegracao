﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VariaveisController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public VariaveisController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Variaveis
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Variavel>>> GetVariavel()
        {
            return await _context.Variavel.ToListAsync();
        }

        // GET: api/Variaveis/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Variavel>> GetVariavel(int id)
        {
            var variavel = await _context.Variavel.FindAsync(id);

            if (variavel == null)
            {
                return NotFound();
            }

            return variavel;
        }

        // PUT: api/Variaveis/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVariavel(int id, Variavel variavel)
        {
            if (id != variavel.Id)
            {
                return BadRequest();
            }

            _context.Entry(variavel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VariavelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Variaveis
        [HttpPost]
        public async Task<ActionResult<Variavel>> PostVariavel(Variavel variavel)
        {
            _context.Variavel.Add(variavel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVariavel", new { id = variavel.Id }, variavel);
        }

        // DELETE: api/Variaveis/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Variavel>> DeleteVariavel(int id)
        {
            var variavel = await _context.Variavel.FindAsync(id);
            if (variavel == null)
            {
                return NotFound();
            }

            _context.Variavel.Remove(variavel);
            await _context.SaveChangesAsync();

            return variavel;
        }

        private bool VariavelExists(int id)
        {
            return _context.Variavel.Any(e => e.Id == id);
        }
    }
}
