﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;
using APIIntegracao.ModelBinding;
using APIIntegracao.Services;
using System.Data;
using System.Data.SqlClient;
using APIIntegracao.Services.ImportModels;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExecucoesController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public ExecucoesController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Execucaos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Execucao>>> GetExecucao()
        {
            return await _context.Execucao.Include(e => e.Rotina).OrderByDescending(e => e.DataHoraInicio).ToListAsync();
        }

        // GET: api/Execucaos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Execucao>> GetExecucao(int id)
        {
            var execucao = await _context.Execucao.FindAsync(id);

            if (execucao == null)
            {
                return NotFound();
            }

            return execucao;
        }

        // PUT: api/Execucaos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExecucao(int id, Execucao execucao)
        {
            if (id != execucao.Id)
            {
                return BadRequest();
            }

            _context.Entry(execucao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExecucaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // https://thomaslevesque.com/2018/09/04/handling-multipart-requests-with-json-and-file-uploads-in-asp-net-core/
        // POST: api/Execucaos
        [HttpPost]
        public async Task<ActionResult<Execucao>> PostExecucao(CreateExecucaoModel post)
        {
            Execucao execucao = new Execucao();
            execucao.Tipo = post.Tipo;
            if (execucao.Tipo == 'A')
            {
                execucao.Arquivo = post.ArquivoFormFile.FileName;
            } else
            {
                execucao.RotinaId = post.RotinaId;
            }
            execucao.DataHoraInicio = DateTime.Now;

            _context.Execucao.Add(execucao);
            await _context.SaveChangesAsync();

            if (execucao.Tipo == 'A')
            {
                FileService _fileService = new FileService(_context);
                await _fileService.ImportarPAD(post.ArquivoFormFile, execucao);
            }
            else if (execucao.Tipo == 'R')
            {
                PostgreService _postgreService = new PostgreService(_context);
                Rotina rotina = _context.Rotina.Find(execucao.RotinaId);
                if (rotina.UrlDestino.Equals("convenios"))
                {
                    await _postgreService.ImportarConvenios(execucao);
                } else if (rotina.UrlDestino.Equals("servidores"))
                {
                    await _postgreService.ImportarServidores(execucao);
                }                    
            }

            return CreatedAtAction("GetExecucao", new { id = execucao.Id }, execucao);

        }

        // DELETE: api/Execucaos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Execucao>> DeleteExecucao(int id)
        {
            var execucao = await _context.Execucao.FindAsync(id);
            if (execucao == null)
            {
                return NotFound();
            }

            _context.Execucao.Remove(execucao);
            await _context.SaveChangesAsync();

            return execucao;
        }

        private bool ExecucaoExists(int id)
        {
            return _context.Execucao.Any(e => e.Id == id);
        }
    }
}
