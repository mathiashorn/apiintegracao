﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TiposConexoesController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public TiposConexoesController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/TiposConexoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoConexao>>> GetTipoConexao()
        {
            return await _context.TipoConexao.ToListAsync();
        }

        // GET: api/TiposConexoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoConexao>> GetTipoConexao(int id)
        {
            var tipoConexao = await _context.TipoConexao.FindAsync(id);

            if (tipoConexao == null)
            {
                return NotFound();
            }

            return tipoConexao;
        }

        // PUT: api/TiposConexoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoConexao(int id, TipoConexao tipoConexao)
        {
            if (id != tipoConexao.Id)
            {
                return BadRequest();
            }

            _context.Entry(tipoConexao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoConexaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TiposConexoes
        [HttpPost]
        public async Task<ActionResult<TipoConexao>> PostTipoConexao(TipoConexao tipoConexao)
        {
            _context.TipoConexao.Add(tipoConexao);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipoConexaoExists(tipoConexao.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTipoConexao", new { id = tipoConexao.Id }, tipoConexao);
        }

        // DELETE: api/TiposConexoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TipoConexao>> DeleteTipoConexao(int id)
        {
            var tipoConexao = await _context.TipoConexao.FindAsync(id);
            if (tipoConexao == null)
            {
                return NotFound();
            }

            _context.TipoConexao.Remove(tipoConexao);
            await _context.SaveChangesAsync();

            return tipoConexao;
        }

        private bool TipoConexaoExists(int id)
        {
            return _context.TipoConexao.Any(e => e.Id == id);
        }
    }
}
