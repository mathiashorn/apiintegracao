﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ConexoesController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public ConexoesController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Conexaos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Conexao>>> GetConexao()
        {
            return await _context.Conexao.Include(e => e.TipoConexao).ToListAsync();
        }

        // GET: api/Conexaos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Conexao>> GetConexao(int id)
        {
            var conexao = await _context.Conexao.FindAsync(id);

            if (conexao == null)
            {
                return NotFound();
            }

            return conexao;
        }

        // PUT: api/Conexaos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConexao(int id, Conexao conexao)
        {
            if (id != conexao.Id)
            {
                return BadRequest();
            }

            _context.Entry(conexao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConexaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Conexaos
        [HttpPost]
        public async Task<ActionResult<Conexao>> PostConexao(Conexao conexao)
        {
            _context.Conexao.Add(conexao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConexao", new { id = conexao.Id }, conexao);
        }

        // DELETE: api/Conexaos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Conexao>> DeleteConexao(int id)
        {
            var conexao = await _context.Conexao.FindAsync(id);
            if (conexao == null)
            {
                return NotFound();
            }

            _context.Conexao.Remove(conexao);
            await _context.SaveChangesAsync();

            return conexao;
        }

        private bool ConexaoExists(int id)
        {
            return _context.Conexao.Any(e => e.Id == id);
        }
    }
}
