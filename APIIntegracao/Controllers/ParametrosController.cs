﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ParametrosController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public ParametrosController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Parametros
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Parametro>>> GetParametro()
        {
            return await _context.Parametro.ToListAsync();
        }

        // GET: api/Parametros/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Parametro>> GetParametro(int id)
        {
            var parametro = await _context.Parametro.FindAsync(id);

            if (parametro == null)
            {
                return NotFound();
            }

            return parametro;
        }

        // PUT: api/Parametros/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParametro(int id, Parametro parametro)
        {
            if (id != parametro.Id)
            {
                return BadRequest();
            }

            _context.Entry(parametro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Parametros
        [HttpPost]
        public async Task<ActionResult<Parametro>> PostParametro(Parametro parametro)
        {
            _context.Parametro.Add(parametro);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParametro", new { id = parametro.Id }, parametro);
        }

        // DELETE: api/Parametros/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Parametro>> DeleteParametro(int id)
        {
            var parametro = await _context.Parametro.FindAsync(id);
            if (parametro == null)
            {
                return NotFound();
            }

            _context.Parametro.Remove(parametro);
            await _context.SaveChangesAsync();

            return parametro;
        }

        private bool ParametroExists(int id)
        {
            return _context.Parametro.Any(e => e.Id == id);
        }
    }
}
