﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIIntegracao.Models;
using Microsoft.AspNetCore.Authorization;
using APIIntegracao.Services.ImportModels;

namespace APIIntegracao.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AjudaController : ControllerBase
    {
        private readonly APIIntegracaoContext _context;

        public AjudaController(APIIntegracaoContext context)
        {
            _context = context;
        }

        // GET: api/Ajuda/convenios
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Ajuda>>> GetAjuda(string id)
        {
            System.Reflection.PropertyInfo[] properties;
            if (id.Equals("convenios"))
            {
                properties = new Convenio().GetType().GetProperties();
            } else if (id.Equals("empenhos"))
            {
                properties = new Empenho().GetType().GetProperties();
            }
            else if (id.Equals("licitacoes"))
            {
                    properties = new Licitacao().GetType().GetProperties();
            }
            else if (id.Equals("servidores"))
            {
                properties = new Servidor().GetType().GetProperties();
            } else
            {
                properties = null;
            }

            if (properties != null)
            {
                List<Ajuda> list = new List<Ajuda>();

                foreach (var p in properties)
                {
                    Ajuda ajuda = new Ajuda();
                    ajuda.Name = p.Name;
                    if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        ajuda.Type = p.PropertyType.GetGenericArguments()[0].Name;
                    } else
                    {
                        ajuda.Type = p.PropertyType.Name;
                    }
                    ajuda.Nullable = IsNullable(p.PropertyType);

                    list.Add(ajuda);

                }

                return list;
            } else
            {
                return null;
            }

        }

        public bool IsNullable<T>(T value)
        {
            return Nullable.GetUnderlyingType(typeof(T)) != null;
        }
    }
}
