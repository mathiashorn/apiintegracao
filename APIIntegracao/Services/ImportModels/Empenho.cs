﻿using System;

namespace APIIntegracao.Services.ImportModels
{
    public partial class Empenho
    {
        public int Id { get; set; }
        public string CodigoOrgao { get; set; }
        public string CodigoUnidadeOrcamentaria { get; set; }
        public string CodigoFuncao { get; set; }
        public string CodigoSubfuncao { get; set; }
        public string CodigoPrograma { get; set; }
        public string CodigoProjeto { get; set; }
        public string CodigoRubricaDespesa { get; set; }
        public string CodigoRecursoVinculado { get; set; }
        public string ContrapartidaRecurso { get; set; }
        public string NumeroEmpenho { get; set; }
        public DateTime? DataEmpenho { get; set; }
        public decimal? ValorEmpenho { get; set; }
        public char? SinalValor { get; set; }
        public string CodigoCredor { get; set; }
        public string CaracteristicaPeculiar { get; set; }
        public char? RegistroPrecos { get; set; }
        public string NumeroLicitacao { get; set; }
        public int? AnoLicitacao { get; set; }
        public string HistoricoEmpenho { get; set; }
        public string ModalidadeLicitacao { get; set; }
        public string BaseLegal { get; set; }
        public char? IdentificadorDespesaFuncionario { get; set; }
        public char? LicitacaoCompartilhada { get; set; }
        public string CnpjGerenciadorLicitacao { get; set; }
    }
}
