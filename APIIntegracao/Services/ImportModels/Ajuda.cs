﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIIntegracao.Services.ImportModels
{
    public partial class Ajuda
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Nullable { get; set; }
     }
}
