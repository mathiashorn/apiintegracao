﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIIntegracao.Services.ImportModels
{
    public partial class Licitacao
    {
        public int Id { get; set; }
        public string CodigoOrgao { get; set; }
        public string NrLicitacao { get; set; }
        public int? AnoLicitacao { get; set; }
        public string CdTipoModalidade { get; set; }
        public int? NrComissao { get; set; }
        public int? AnoComissao { get; set; }
        public char? TpComissao { get; set; }
        public string NrProcesso { get; set; }
        public int? AnoProcesso { get; set; }
        public string TpObjeto { get; set; }
        public string CdTipoFaseAtual { get; set; }
        public string TpLicitacao { get; set; }
        public char? TpNivelJulgamento { get; set; }
        public DateTime? DtAutorizacaoAdesao { get; set; }
        public string TpCaracteristicaObjeto { get; set; }
        public char? TpNatureza { get; set; }
        public char? TpRegimeExecucao { get; set; }
        public char? BlPermiteSubcontratacao { get; set; }
        public char? TpBeneficioMicroEpp { get; set; }
        public char? TpFornecimento { get; set; }
        public char? TpAtuacaoRegistro { get; set; }
        public string NrLicitacaoOriginal { get; set; }
        public int? AnoLicitacaoOriginal { get; set; }
        public string NrAtaRegistroPreco { get; set; }
        public DateTime? DtAtaRegistroPreco { get; set; }
        public decimal? PcTaxaRisco { get; set; }
        public char? TpExecucao { get; set; }
        public char? TpDisputa { get; set; }
        public char? TpPrequalificacao { get; set; }
        public char? BlInversaoFases { get; set; }
        public char? TpResultadoGlobal { get; set; }
        public string CnpjOrgaoGerenciador { get; set; }
        public string NmOrgaoGerenciador { get; set; }
        public string DsObjeto { get; set; }
        public string CdTipoFundamentacao { get; set; }
        public int? NrArtigo { get; set; }
        public string DsInciso { get; set; }
        public string DsLei { get; set; }
        public DateTime? DtInicioInscrCred { get; set; }
        public DateTime? DtFimInscrCred { get; set; }
        public DateTime? DtInicioVigenCred { get; set; }
        public DateTime? DtFimVigenCred { get; set; }
        public decimal? VlLicitacao { get; set; }
        public char? BlOrcamentoSigiloso { get; set; }
        public char? BlRecebeInscricaoPerVig { get; set; }
        public char? BlPermiteConsorcio { get; set; }
        public DateTime? DtAbertura { get; set; }
        public DateTime? DtHomologacao { get; set; }
        public DateTime? DtAdjudicacao { get; set; }
        public char? BlLicitPropriaOrgao { get; set; }
        public char? TpDocumentoFornecedor { get; set; }
        public string NrDocumentoFornecedor { get; set; }
        public char? TpDocumentoVencedor { get; set; }
        public string NrDocumentoVencedor { get; set; }
        public decimal? VlHomologado { get; set; }
        public char? BlGeraDespesa { get; set; }
        public string DsObservacao { get; set; }
        public decimal? PcTxEstimada { get; set; }
        public decimal? PcTxHomologada { get; set; }
        public char? BlCompartilhada { get; set; }
    }
}
