﻿using APIIntegracao.Models;
using System.Linq;

namespace APIIntegracao.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly APIIntegracaoContext _context;

        public UserManagementService(APIIntegracaoContext context)
        {
            _context = context;
        }

        public bool IsValidUser(string userName, string password)
        {
            var user = _context.Usuario
                .FirstOrDefault(u => u.Email == userName
                             && u.Senha == password);

            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
