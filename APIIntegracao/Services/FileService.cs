﻿using APIIntegracao.Models;
using APIIntegracao.Services.ImportModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace APIIntegracao.Services
{
    public class FileService
    {
        private readonly APIIntegracaoContext _context;

        public FileService(APIIntegracaoContext context)
        {
            this._context = context;
        }

        public async Task ImportarPAD(IFormFile arquivo, Execucao execucao)
        {

            if (arquivo.FileName.Equals("EMPENHO.TXT"))
            {
                await ImportarEmpenho(arquivo, execucao);
            }
            else if (arquivo.FileName.Equals("licitacao.csv"))
            {
                await ImportarLicitacao(arquivo, execucao);
            }

        }

        private async Task ImportarEmpenho(IFormFile arquivo, Execucao execucao)
        {

            try
            {
                // extrair informações do arquivo e popular o list

                List<Empenho> empenhos = new List<Empenho>();
                var result = new StringBuilder();
                var ci1 = new CultureInfo("pt-BR");
                using (var reader = new StreamReader(arquivo.OpenReadStream(), Encoding.GetEncoding("iso-8859-1")))
                {

                    reader.Peek();
                    String header = await reader.ReadLineAsync();
                    String line;

                    while (reader.Peek() >= 0)
                    {
                        line = await reader.ReadLineAsync();

                        if (!line.Substring(0, 11).Equals("FINALIZADOR"))
                        {
                            Empenho empenho = new Empenho();
                            empenho.CodigoOrgao = line.Substring(0, 2);
                            empenho.CodigoUnidadeOrcamentaria = line.Substring(2, 2);
                            empenho.CodigoFuncao = line.Substring(4, 2);
                            empenho.CodigoSubfuncao = line.Substring(6, 3);
                            empenho.CodigoPrograma = line.Substring(9, 4);
                            // espaço em branco
                            empenho.CodigoProjeto = line.Substring(16, 5);
                            empenho.CodigoRubricaDespesa = line.Substring(21, 15);
                            empenho.CodigoRecursoVinculado = line.Substring(36, 4);
                            empenho.ContrapartidaRecurso = line.Substring(40, 4);
                            empenho.NumeroEmpenho = line.Substring(44, 13);
                            empenho.DataEmpenho = DateTime.ParseExact(line.Substring(57, 8), "ddmmyyyy", null);
                            empenho.ValorEmpenho = Decimal.Parse(line.Substring(65, 13));
                            empenho.SinalValor = line.Substring(78, 1).ToCharArray()[0];
                            empenho.CodigoCredor = line.Substring(79, 10);
                            // espaço em branco
                            empenho.CaracteristicaPeculiar = line.Substring(254, 3);
                            // espaço em branco
                            empenho.RegistroPrecos = line.Substring(259, 1).ToCharArray()[0];
                            // espaço em branco
                            empenho.NumeroLicitacao = int.Parse(line.Substring(280, 20)).ToString();
                            empenho.AnoLicitacao = int.Parse(line.Substring(300, 4));
                            empenho.HistoricoEmpenho = line.Substring(304, 400);
                            empenho.ModalidadeLicitacao = line.Substring(704, 3);
                            empenho.BaseLegal = line.Substring(707, 2);
                            empenho.IdentificadorDespesaFuncionario = line.Substring(709, 1).ToCharArray()[0];
                            //empenho.LicitacaoCompartilhada = line.Substring(710,1).ToCharArray()[0];
                            //empenho.CnpjGerenciadorLicitacao = line.Substring(711,14);

                            empenhos.Add(empenho);
                        }

                    }

                }

                // postar na API do Portal da Transparencia

                HttpClient client = new HttpClient();
                string endereco = _context.Parametro.Where(p => p.Chave.Equals("enderecoPortalTransparencia")).FirstOrDefault().Valor;

                client.BaseAddress = new Uri(endereco);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string usuario = _context.Parametro.Where(p => p.Chave.Equals("usuarioPortalTransparencia")).FirstOrDefault().Valor;
                string senha = _context.Parametro.Where(p => p.Chave.Equals("senhaPortalTransparencia")).FirstOrDefault().Valor;

                var jsonContent = JsonConvert.SerializeObject(new { username = usuario, password = senha });
                var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync("authentication/request", contentString);

                jsonContent = JsonConvert.SerializeObject(empenhos);
                contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                TokenResponse tokenResponse = await response.Content.ReadAsAsync<TokenResponse>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResponse.Token);

                response = await client.PostAsync("empenhos", contentString);

                // gravar como OK/Sucesso

                execucao.DataHoraTermino = DateTime.Now;
                execucao.Status = "OK";
                execucao.MensagemRetorno = "Sucesso";

                await _context.SaveChangesAsync();


            }
            catch (Exception ex)
            {
                execucao.Status = "Erro";
                execucao.MensagemRetorno = ex.Message;

                _context.SaveChangesAsync();
            }

        }

        private async Task ImportarLicitacao(IFormFile arquivo, Execucao execucao)
        {
            String line;
            try
            {
                // extrair informações do arquivo e popular o list

                List<Licitacao> licitacoes = new List<Licitacao>();
                var result = new StringBuilder();
                var ci1 = new CultureInfo("pt-BR");
                using (var reader = new StreamReader(arquivo.OpenReadStream(), Encoding.GetEncoding("iso-8859-1")))
                {

                    reader.Peek();
                    String header = await reader.ReadLineAsync();

                    while (reader.Peek() >= 0)
                    {
                        line = await reader.ReadLineAsync();

                        if (!line.Equals(""))
                        {

                            // substitui vírgulas dentro das aspas duplas por outro caractere, para manter o número correto
                            // de colunas
                            if (line.IndexOf("\"") > 0)
                            {

                                string[] arraySemVirgulaNaDesc = line.Split('"');
                                bool inicio = true;
                                line = "";
                                foreach (var item in arraySemVirgulaNaDesc)
                                {
                                    inicio = !inicio;
                                    if (inicio)
                                    {
                                        // se estiver no meio de aspas duplas, substitui a vírgula
                                        line += item.Replace(",", "ɐ");
                                    } else
                                    {
                                        line += item;
                                    }
                                    
                                }                               
                            }                            
                            string[] array = line.Split(',');

                            Licitacao licitacao = new Licitacao();
                            licitacao.CodigoOrgao = array[0];
                            //licitacao.NomeOrgao = array[1];
                            licitacao.NrLicitacao = array[2];
                            licitacao.AnoLicitacao = (array[3].Equals(String.Empty) ? null : (int?)int.Parse(array[3]));
                            licitacao.CdTipoModalidade = array[4];
                            licitacao.NrComissao = (array[5].Equals(String.Empty) ? null : (int?)int.Parse(array[5]));
                            licitacao.AnoComissao = (array[6].Equals(String.Empty) ? null : (int?)int.Parse(array[6]));
                            licitacao.TpComissao = (array[7].Equals(String.Empty) ? null : (char?)char.Parse(array[7]));
                            licitacao.NrProcesso = array[8];
                            licitacao.AnoProcesso = (array[9].Equals(String.Empty) ? null : (int?)int.Parse(array[9]));
                            licitacao.TpObjeto = array[10];
                            licitacao.CdTipoFaseAtual = array[11];
                            licitacao.TpLicitacao = array[12];
                            licitacao.TpNivelJulgamento = (array[13].Equals(String.Empty) ? null : (char?)char.Parse(array[13]));
                            licitacao.DtAutorizacaoAdesao = (array[14].ToString().Equals(String.Empty) ? null : (DateTime?) DateTime.ParseExact(array[14], "yyyy-mm-dd", null));
                            licitacao.TpCaracteristicaObjeto = array[15];
                            licitacao.TpNatureza = (array[16].Equals(String.Empty) ? null : (char?)char.Parse(array[16]));
                            licitacao.TpRegimeExecucao = (array[17].Equals(String.Empty) ? null : (char?) char.Parse(array[17]));
                            licitacao.BlPermiteSubcontratacao = (array[18].Equals(String.Empty) ? null : (char?)char.Parse(array[18]));
                            licitacao.TpBeneficioMicroEpp = (array[19].Equals(String.Empty) ? null : (char?)char.Parse(array[19]));
                            licitacao.TpFornecimento = (array[20].Equals(String.Empty) ? null : (char?)char.Parse(array[20]));
                            licitacao.TpAtuacaoRegistro = (array[21].Equals(String.Empty) ? null : (char?)char.Parse(array[21]));
                            licitacao.NrLicitacaoOriginal = array[22];
                            licitacao.AnoLicitacaoOriginal = (array[23].Equals(String.Empty) ? null : (int?)int.Parse(array[23]));
                            licitacao.NrAtaRegistroPreco = array[24];
                            licitacao.DtAtaRegistroPreco = (array[25].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[25], "yyyy-mm-dd", null));
                            licitacao.PcTaxaRisco = (array[26].Equals(String.Empty) ? null : (decimal?)decimal.Parse(array[26]));
                            licitacao.TpExecucao = (array[27].Equals(String.Empty) ? null : (char?)char.Parse(array[27]));
                            licitacao.TpDisputa = (array[28].Equals(String.Empty) ? null : (char?)char.Parse(array[28]));
                            licitacao.TpPrequalificacao = (array[29].Equals(String.Empty) ? null : (char?)char.Parse(array[29]));
                            licitacao.BlInversaoFases = (array[30].Equals(String.Empty) ? null : (char?)char.Parse(array[30]));
                            licitacao.TpResultadoGlobal = (array[31].Equals(String.Empty) ? null : (char?)char.Parse(array[31]));
                            licitacao.CnpjOrgaoGerenciador = array[32];
                            licitacao.NmOrgaoGerenciador = array[33];
                            licitacao.DsObjeto = array[34].Replace("ɐ", ",");
                            licitacao.CdTipoFundamentacao = array[35];
                            licitacao.NrArtigo = (array[36].Equals(String.Empty) ? null : (int?)int.Parse(array[36]));
                            licitacao.DsInciso = array[37];
                            licitacao.DsLei = array[38];
                            licitacao.DtInicioInscrCred = (array[39].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[39], "yyyy-mm-dd", null));
                            licitacao.DtFimInscrCred = (array[40].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[40], "yyyy-mm-dd", null));
                            licitacao.DtInicioVigenCred = (array[41].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[41], "yyyy-mm-dd", null));
                            licitacao.DtFimVigenCred = (array[42].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[42], "yyyy-mm-dd", null));
                            licitacao.VlLicitacao = (array[43].Equals(String.Empty) ? null : ((decimal?)decimal.Parse(array[43]) / 100));
                            licitacao.BlOrcamentoSigiloso = (array[44].Equals(String.Empty) ? null : (char?)char.Parse(array[44]));
                            licitacao.BlRecebeInscricaoPerVig = (array[45].Equals(String.Empty) ? null : (char?)char.Parse(array[45]));
                            licitacao.BlPermiteConsorcio = (array[46].Equals(String.Empty) ? null : (char?)char.Parse(array[46]));
                            licitacao.DtAbertura = (array[47].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[47], "yyyy-mm-dd", null));
                            licitacao.DtHomologacao = (array[48].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[48], "yyyy-mm-dd", null));
                            licitacao.DtAdjudicacao = (array[49].ToString().Equals(String.Empty) ? null : (DateTime?)DateTime.ParseExact(array[49], "yyyy-mm-dd", null));
                            licitacao.BlLicitPropriaOrgao = (array[50].Equals(String.Empty) ? null : (char?)char.Parse(array[50]));
                            licitacao.TpDocumentoFornecedor = (array[51].Equals(String.Empty) ? null : (char?)char.Parse(array[51]));
                            licitacao.NrDocumentoFornecedor = array[52];
                            licitacao.TpDocumentoVencedor = (array[53].Equals(String.Empty) ? null : (char?)char.Parse(array[53]));
                            licitacao.NrDocumentoVencedor = array[54];
                            licitacao.VlHomologado = (array[55].Equals(String.Empty) ? null : ((decimal?)decimal.Parse(array[55])/100));
                            licitacao.BlGeraDespesa = (array[56].Equals(String.Empty) ? null : (char?)char.Parse(array[56]));
                            licitacao.DsObservacao = array[57];
                            licitacao.PcTxEstimada = (array[58].Equals(String.Empty) ? null : (decimal?)decimal.Parse(array[58]));
                            licitacao.PcTxHomologada = (array[59].Equals(String.Empty) ? null : (decimal?)decimal.Parse(array[59]));
                            licitacao.BlCompartilhada = (array[60].Equals(String.Empty) ? null : (char?)char.Parse(array[60]));

                            licitacoes.Add(licitacao);
                        }

                    }

                }

                // postar na API do Portal da Transparencia

                HttpClient client = new HttpClient();
                string endereco = _context.Parametro.Where(p => p.Chave.Equals("enderecoPortalTransparencia")).FirstOrDefault().Valor;

                client.BaseAddress = new Uri(endereco);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string usuario = _context.Parametro.Where(p => p.Chave.Equals("usuarioPortalTransparencia")).FirstOrDefault().Valor;
                string senha = _context.Parametro.Where(p => p.Chave.Equals("senhaPortalTransparencia")).FirstOrDefault().Valor;

                var jsonContent = JsonConvert.SerializeObject(new { username = usuario, password = senha });
                var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync("authentication/request", contentString);

                jsonContent = JsonConvert.SerializeObject(licitacoes);
                contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                TokenResponse tokenResponse = await response.Content.ReadAsAsync<TokenResponse>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResponse.Token);

                response = await client.PostAsync("licitacoes", contentString);

                // gravar como OK/Sucesso

                execucao.DataHoraTermino = DateTime.Now;
                execucao.Status = "OK";
                execucao.MensagemRetorno = "Sucesso";

                await _context.SaveChangesAsync();


            }
            catch (Exception ex)
            {
                execucao.Status = "Erro";
                execucao.MensagemRetorno = ex.Message;

                _context.SaveChangesAsync();
            }

        }

    }

}

