﻿using APIIntegracao.Models;
using APIIntegracao.Services.ImportModels;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace APIIntegracao.Services
{
    public class PostgreService
    {
        private readonly APIIntegracaoContext _context;

        public PostgreService(APIIntegracaoContext context)
        {
            this._context = context;
        }

        public async Task ImportarConvenios(Execucao execucao)
        {

            try
            {

                Rotina rotina = _context.Rotina.Where(r => r.Id == execucao.RotinaId).Include(r => r.Conexao).FirstOrDefault();
                PostgreSQLContext postgreSQLContext = new PostgreSQLContext(rotina.Conexao.ConnectionString);
                List<Convenio> convenios = new List<Convenio>();

                using (var command = postgreSQLContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = rotina.Consulta;
                    command.CommandType = CommandType.Text;
                    //var parameter = new SqlParameter("@p1",...);
                    //command.Parameters.Add(parameter);

                    postgreSQLContext.Database.OpenConnection();

                    using (var result = command.ExecuteReader())
                    {
                        while (result.Read())
                        {

                            Convenio convenio = new Convenio();
                            convenio.Convenio1 = result.GetString(1);
                            convenio.Administracao = result.GetString(2);
                            convenio.Situacao = result.GetChar(3);
                            convenio.Convenente = result.GetString(4);
                            convenio.Concedente = result.GetString(5);
                            convenio.Interveniente = (result.IsDBNull(6) ? "" : result.GetString(6));
                            convenio.Tipo = result.GetString(7);
                            convenio.Classificacao = result.GetString(8);
                            convenio.Objeto = result.GetString(9);
                            convenio.DataAssinatura = (result.IsDBNull(10) ? null : (DateTime?) result.GetDateTime(10));
                            convenio.DataEncerramento = (result.IsDBNull(11) ? null : (DateTime?)result.GetDateTime(11));
                            convenio.DataRescisao = (result.IsDBNull(12) ? null : (DateTime?)result.GetDateTime(12));
                            convenio.DataInicio = (result.IsDBNull(13) ? null : (DateTime?)result.GetDateTime(13));
                            convenio.DataVencimento = (result.IsDBNull(14) ? null : (DateTime?)result.GetDateTime(14));
                            convenio.DataVencimentoOriginal = (result.IsDBNull(15) ? null : (DateTime?)result.GetDateTime(15));
                            convenio.Recurso = result.GetString(16);
                            convenio.Contrapartida = (result.IsDBNull(17) ? "" : result.GetString(17));
                            convenio.ValorRecurso = result.GetDecimal(18);
                            convenio.ValorContrapartida = result.GetDecimal(19);
                            convenio.ValorOriginal = result.GetDecimal(20);
                            convenio.ValorTotal = result.GetDecimal(21);

                            convenios.Add(convenio);

                        }
                    }
                }

                // postar na API do Portal da Transparencia

                HttpClient client = new HttpClient();
                string endereco = _context.Parametro.Where(p => p.Chave.Equals("enderecoPortalTransparencia")).FirstOrDefault().Valor;

                client.BaseAddress = new Uri(endereco);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string usuario = _context.Parametro.Where(p => p.Chave.Equals("usuarioPortalTransparencia")).FirstOrDefault().Valor;
                string senha = _context.Parametro.Where(p => p.Chave.Equals("senhaPortalTransparencia")).FirstOrDefault().Valor;

                var jsonContent = JsonConvert.SerializeObject(new { username = usuario, password = senha });
                var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync("authentication/request", contentString);

                jsonContent = JsonConvert.SerializeObject(convenios);
                contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                TokenResponse tokenResponse = await response.Content.ReadAsAsync<TokenResponse>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResponse.Token);

                response = await client.PostAsync(rotina.UrlDestino, contentString);

                // gravar como OK/Sucesso

                execucao.DataHoraTermino = DateTime.Now;
                execucao.Status = "OK";
                execucao.MensagemRetorno = "Sucesso";

                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                execucao.Status = "Erro";
                execucao.MensagemRetorno = ex.Message;

                _context.SaveChangesAsync();
            }

        }

        public async Task ImportarServidores(Execucao execucao)
        {

            try
            {

                Rotina rotina = _context.Rotina.Where(r => r.Id == execucao.RotinaId).Include(r => r.Conexao).FirstOrDefault();
                PostgreSQLContext postgreSQLContext = new PostgreSQLContext(rotina.Conexao.ConnectionString);
                List<Servidor> servidores = new List<Servidor>();

                using (var command = postgreSQLContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = rotina.Consulta;
                    command.CommandType = CommandType.Text;
                    //var parameter = new SqlParameter("@p1",...);
                    //command.Parameters.Add(parameter);

                    postgreSQLContext.Database.OpenConnection();

                    using (var result = command.ExecuteReader())
                    {
                        while (result.Read())
                        {
                            Servidor servidor = new Servidor();
                            servidor.Matricula = result.GetInt32(1);
                            servidor.Nome = result.GetString(2);
                            servidor.CodigoCargo = result.GetInt32(3);
                            servidor.NomeCargo = result.GetString(4);
                            servidor.Cpf = result.GetString(5);
                            servidor.CodigoTipoMov = result.GetInt32(6);
                            servidor.DescTipoMov = result.GetString(7);
                            servidor.HorasMensais = result.GetInt32(8);
                            servidor.DataAdmissao = (result.IsDBNull(9) ? null : (DateTime?)result.GetDateTime(9));
                            servidor.CodigoSetor = result.GetInt32(10);
                            servidor.NomeSetor = result.GetString(11);
                            servidor.CodigoDivisao = result.GetInt32(12);
                            servidor.NomeDivisao = result.GetString(13);
                            servidor.CodigoOrgao = result.GetInt32(14);
                            servidor.NomeOrgao = result.GetString(15);
                            servidor.CodigoCentroCusto = result.GetInt32(16);
                            servidor.NomeCentroCusto = result.GetString(17);
                            servidor.CodigoVinculo = result.GetInt32(18);
                            servidor.NomeVinculo = result.GetString(19);
                            servidor.Padrao = result.GetString(20);
                            servidor.DataReferencia = (result.IsDBNull(21) ? null : (DateTime?)result.GetDateTime(21));
                            servidor.Inativo = result.GetChar(22);
                            servidor.Pensionista = result.GetChar(23);
                            servidor.TempoServico = result.GetInt32(24);
                            servidor.ValorRemuneracaoBasica = result.GetDecimal(25);
                            servidor.ValorVerbasEventuais = result.GetDecimal(26);
                            servidor.ValorVerbasIndenizatorias = result.GetDecimal(27);
                            servidor.ValorFerias = result.GetDecimal(28);
                            servidor.ValorDecimoTerceiro = result.GetDecimal(29);
                            servidor.ValorDeducoesObrigatorias = result.GetDecimal(30);
                            servidor.ValorRemuneracao = result.GetDecimal(31);

                            servidores.Add(servidor);

                        }
                    }
                }

                // postar na API do Portal da Transparencia

                HttpClient client = new HttpClient();
                string endereco = _context.Parametro.Where(p => p.Chave.Equals("enderecoPortalTransparencia")).FirstOrDefault().Valor;

                client.BaseAddress = new Uri(endereco);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                string usuario = _context.Parametro.Where(p => p.Chave.Equals("usuarioPortalTransparencia")).FirstOrDefault().Valor;
                string senha = _context.Parametro.Where(p => p.Chave.Equals("senhaPortalTransparencia")).FirstOrDefault().Valor;

                var jsonContent = JsonConvert.SerializeObject(new { username = usuario, password = senha });
                var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync("authentication/request", contentString);

                jsonContent = JsonConvert.SerializeObject(servidores);
                contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                contentString.Headers.ContentType = new
                MediaTypeHeaderValue("application/json");

                TokenResponse tokenResponse = await response.Content.ReadAsAsync<TokenResponse>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResponse.Token);

                response = await client.PostAsync(rotina.UrlDestino, contentString);

                // gravar como OK/Sucesso

                execucao.DataHoraTermino = DateTime.Now;
                execucao.Status = "OK";
                execucao.MensagemRetorno = "Sucesso";

                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                execucao.Status = "Erro";
                execucao.MensagemRetorno = ex.Message;

                _context.SaveChangesAsync();
            }

        }

    }

}

