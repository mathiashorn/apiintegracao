﻿using APIIntegracao.Models;
using System.Threading.Tasks;

namespace APIIntegracao.Services
{
    public interface IFileService
    {
        Task ReadTextAsync(string filePath, Execucao execucao);
    }
}
