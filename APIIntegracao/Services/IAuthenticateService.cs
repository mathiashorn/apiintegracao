﻿using APIIntegracao.Models;
using System;

namespace APIIntegracao.Services
{
    public interface IAuthenticateService
    {
        bool IsAuthenticated(TokenRequest request, out string token, out DateTime? expires);
    }
}
