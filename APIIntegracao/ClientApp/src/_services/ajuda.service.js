﻿import axios from 'axios';
import config from '../config/config';

export const ajudaService = {
    get
};
function get(apiEndpoint) {
    return axios.get(config.baseUrl + apiEndpoint, getOptions())
        .then((response) => {
            return response;
        }).catch((err) => {
            console.log(err);
        })
}
function getOptions() {
    let options = {};
    if (localStorage.getItem('token')) {
        options.headers = { 'Authorization': 'Bearer ' + localStorage.getItem('token') };
    }
    return options;
}