﻿import axios from 'axios';
import config from '../config/config';

export const authServices = {
    post
};
function post(apiEndpoint, payload) {
    return axios.post(config.baseUrl + apiEndpoint, payload)
        .then((response) => {
            return response;
        }).catch((err) => {
            console.log(err);
        })
}
