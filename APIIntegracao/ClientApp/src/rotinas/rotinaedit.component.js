﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Nav from '../_components/nav';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { rotinaAction, conexaoAction, ajudaAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
    panel: {
        backgroundColor: '#fff9c4'
    }
});
class RotinaEdit extends Component {

    handleChangeSelectedDestino(value) {
        this.props.rotina.urlDestino = value;
        this.forceUpdate();

        const { dispatch } = this.props;
        dispatch(ajudaAction.getAjuda(value));

    }

    handleChangeSelectedConexao(value) {
        this.props.rotina.conexaoId = value;
        this.forceUpdate();
    }

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(rotinaAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        const { dispatch } = this.props;
        dispatch(conexaoAction.getConexao());
        if (params.id) {
            dispatch(rotinaAction.getRotinaById(params.id));
        }
    }

    handleClick(event) {

        const { match: { params } } = this.props;
        const { dispatch } = this.props;

        let payload = {
            id: params.id,
            conexaoId: this.props.rotina.conexaoId,
            descricao: this.props.rotina.descricao,
            consulta: this.props.rotina.consulta,
            urlDestino: this.props.rotina.urlDestino
        }

        if (params.id) {
            dispatch(rotinaAction.editRotinaInfo(params.id, payload));
        } else {
            dispatch(rotinaAction.createRotina(payload));
        }
    }

    renderOptionsConexao() {
        return this.props.conexao.conexao.map((p, i) => {
            return (
                <MenuItem
                    label="Selecione ..."
                    value={p.id}
                    key={i} name={p.descricao}>{p.descricao}
                </MenuItem>

            );
        });
    }

    renderOptionsDestino() {
        var destinoData = [
            { value: 'convenios', name: 'Convênios' },
            { value: 'empenhos', name: 'Empenhos' },
            { value: 'licitacoes', name: 'Licitações' },
            { value: 'servidores', name: 'Servidores' },
        ];
        return destinoData.map((p, i) => {
            return (
                <MenuItem
                    label="Selecione ..."
                    value={p.value}
                    key={i} name={p.name}>{p.name}
                </MenuItem>
            );
        });
    };

    render() {

        const { classes } = this.props;
        const { match: { params } } = this.props;
        const { ajuda } = this.props.ajuda;

        function InsertText(props) {
            return <Typography variant="h2"><b>{'Nova Rotina'}</b></Typography>;
        }

        function EditText(props) {
            return <Typography variant="h2"><b>{'Editar Rotina'}</b></Typography>;
        }
        function SegHeader() {
            if (params.id) {
                return <EditText />;
            }
            return <InsertText />;
        }
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={24}>
                            <Grid item container xs={3}>
                                <SegHeader />
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <form className={classes.container}>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="conexao">Conexão</InputLabel>
                                                        <Select
                                                            name="conexao"
                                                            value={this.props.rotina.conexaoId}
                                                            onChange={event => this.handleChangeSelectedConexao(event.target.value)}
                                                            input={<Input id="conexao" />}
                                                        >
                                                            {this.renderOptionsConexao()}
                                                        </Select>
                                                        {/*<FormHelperText>Label + placeholder</FormHelperText>*/}
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="urlDestino">Destino</InputLabel>
                                                        <Select
                                                            name="urlDestino"
                                                            value={this.props.rotina.urlDestino}
                                                            onChange={event => this.handleChangeSelectedDestino(event.target.value)}
                                                            input={<Input id="urlDestino" />}
                                                        >
                                                            {this.renderOptionsDestino()}
                                                        </Select>
                                                        {/*<FormHelperText>Label + placeholder</FormHelperText>*/}
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={24}>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="descricao"
                                                            label="Descrição"
                                                            className={classes.textField}
                                                            value={this.props.rotina.descricao}
                                                            onChange={this.handleChange('descricao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <ExpansionPanel className={classes.panel}>
                                                        <ExpansionPanelSummary
                                                            expandIcon={<ExpandMoreIcon />}
                                                            aria-controls="panel1a-content"
                                                            id="panel1a-header"
                                                        >
                                                            <Typography className={classes.heading}>Estrutura dos dados</Typography>
                                                        </ExpansionPanelSummary>
                                                        <ExpansionPanelDetails>
                                                            <Table className={classes.table}>
                                                                <TableHead>
                                                                    <TableRow>
                                                                        <TableCell>Nome</TableCell>
                                                                        <TableCell>Tipo</TableCell>
                                                                        <TableCell>Obrigatório</TableCell>
                                                                    </TableRow>
                                                                </TableHead>
                                                                <TableBody>
                                                                    {
                                                                        ajuda.map(n => {

                                                                            var obrigatorio = '';
                                                                            if (n.nullable) {
                                                                                obrigatorio = 'Sim';
                                                                            } else {
                                                                                obrigatorio = 'Não';
                                                                            }

                                                                            return (
                                                                                <TableRow key={n.id}>
                                                                                    <TableCell component="th" scope="row">
                                                                                        {n.name}
                                                                                    </TableCell>
                                                                                    <TableCell>{n.type}</TableCell>
                                                                                    <TableCell>{obrigatorio}</TableCell>
                                                                                </TableRow>
                                                                            );
                                                                        })
                                                                    }
                                                                </TableBody>
                                                            </Table>
                                                        </ExpansionPanelDetails>
                                                    </ExpansionPanel>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="consulta"
                                                            label="Consulta"
                                                            multiline
                                                            className={classes.textField}
                                                            value={this.props.rotina.consulta}
                                                            onChange={this.handleChange('consulta')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={24}>
                                                <Grid item xs={3}>
                                                </Grid>
                                                <Grid item xs={6}>
                                                </Grid>
                                                <Grid item xs={3} container justify="center">
                                                    <Grid container spacing={24}>
                                                        <Grid item xs={6} container justify="center">
                                                            <Button variant="contained" color="secondary" className={classes.button} component='a' href="/rotinas">Cancelar</Button>
                                                        </Grid>
                                                        <Grid item xs={6} container justify="flex-start">
                                                            <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)}>Salvar</Button>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
RotinaEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedRotinaEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(RotinaEdit)));
export { connectedRotinaEditPage as RotinaEdit };