﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Nav from '../_components/nav';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { execucaoAction, rotinaAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
});
class ExecucaoEdit extends Component {

    handleChangeSelectedTipo(value) {
        this.props.execucao.tipo = value;
        this.forceUpdate();
    }

    handleChangeSelectedRotina(value) {
        this.props.execucao.rotinaId = value;
        this.forceUpdate();
    }

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(execucaoAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        const { dispatch } = this.props;
        dispatch(rotinaAction.getRotina());
        if (params.id) {
            dispatch(execucaoAction.getExecucaoById(params.id));
        }
    }

    handleClick(event) {

        const { match: { params } } = this.props;
        const { dispatch } = this.props;

        var formData = new FormData();
        if (this.props.execucao.ArquivoFormFile) {
            formData.append('ArquivoFormFile', this.props.execucao.ArquivoFormFile);
        }

        let payload = {
            id: params.id,
            tipo: this.props.execucao.tipo,
            rotinaId: this.props.execucao.rotinaId,
            arquivo: this.props.execucao.arquivo,
            dataHoraInicio: this.props.execucao.dataHoraInicio,
            dataHoraTermino: this.props.execucao.dataHoraTermino,
            status: this.props.execucao.status,
            mensagemRetorno: this.props.execucao.mensagemRetorno
        }
        formData.append('json', JSON.stringify(payload));

        if (params.id) {
            dispatch(execucaoAction.editExecucaoInfo(params.id, payload));
        } else {
            //dispatch(execucaoAction.createExecucao(payload));
            dispatch(execucaoAction.createExecucao(formData));
        }
    }

    setFile(e) {
        //this.setState({ ArquivoFormFile: e.target.files[0] });
        this.props.execucao.ArquivoFormFile = e.target.files[0];
        this.forceUpdate();
    }

    renderOptionsTipo() {
        var orderTipo = [
            { value: 'A', name: 'Arquivo' },
            { value: 'R', name: 'Rotina' },
        ];
        return orderTipo.map((p, i) => {
            return (
                <MenuItem
                    label="Selecione ..."
                    value={p.value}
                    key={i} name={p.name}>{p.name}
                </MenuItem>
            );
        });
    };

    renderOptionsRotina() {
        return this.props.rotina.rotina.map((p, i) => {
            return (
                <MenuItem
                    label="Selecione ..."
                    value={p.id}
                    key={i} name={p.descricao}>{p.descricao}
                </MenuItem>

            );
        });
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;
        function InsertText(props) {
            return <Typography variant="h2"><b>{'Nova Execução'}</b></Typography>;
        }

        function EditText(props) {
            return <Typography variant="h2"><b>{'Editar Execução'}</b></Typography>;
        }
        function SegHeader() {
            if (params.id) {
                return <EditText />;
            }
            return <InsertText />;
        }
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={24}>
                            <Grid item container xs={3}>
                                <SegHeader />
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <form className={classes.container}>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="tipo">Tipo</InputLabel>
                                                        <Select
                                                            name="tipo"
                                                            value={this.props.execucao.tipo}
                                                            onChange={event => this.handleChangeSelectedTipo(event.target.value)}
                                                            input={<Input id="tipo" />}
                                                        >
                                                            {this.renderOptionsTipo()}
                                                        </Select>
                                                        {/*<FormHelperText>Label + placeholder</FormHelperText>*/}
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="rotina">Rotina</InputLabel>
                                                        <Select
                                                            name="rotina"
                                                            value={this.props.execucao.rotinaId}
                                                            className={classes.textField}
                                                            onChange={event => this.handleChangeSelectedRotina(event.target.value)}
                                                            input={<Input id="rotina" />}
                                                            margin="normal"
                                                            disabled={(this.props.execucao.tipo == 'A') ? "disabled" : ""}
                                                        >
                                                            {this.renderOptionsRotina()}
                                                        </Select>
                                                        {/*<FormHelperText>Label + placeholder</FormHelperText>*/}
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="arquivo"
                                                            label="Arquivo"
                                                            className={classes.textField}
                                                            onChange={e => this.setFile(e)}
                                                            margin="normal"
                                                            disabled={(this.props.execucao.tipo == 'R') ? "disabled" : ""}
                                                            type="file"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataHoraInicio"
                                                            label="Data/Hora Início"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            defaultValue="2017-05-24T10:30"
                                                            value={this.props.execucao.dataHoraInicio}
                                                            onChange={this.handleChange('dataHoraInicio')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataHoraTermino"
                                                            label="Data/Hora Fim"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            defaultValue="2017-05-24T10:30"
                                                            value={this.props.execucao.dataHoraTermino}
                                                            onChange={this.handleChange('dataHoraTermino')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="status"
                                                            label="Status"
                                                            className={classes.textField}
                                                            value={this.props.execucao.status}
                                                            onChange={this.handleChange('status')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="mensagemRetorno"
                                                            label="Mensagem de Retorno"
                                                            multiline
                                                            rows="4"
                                                            className={classes.textField}
                                                            value={this.props.execucao.mensagemRetorno}
                                                            onChange={this.handleChange('mensagemRetorno')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={24}>
                                                <Grid item xs={3}>
                                                </Grid>
                                                <Grid item xs={6}>
                                                </Grid>
                                                <Grid item xs={3} container justify="center">
                                                    <Grid container spacing={24}>
                                                        <Grid item xs={6} container justify="center">
                                                            <Button variant="contained" color="secondary" className={classes.button} component='a' href="/execucoes">Cancelar</Button>
                                                        </Grid>
                                                        <Grid item xs={6} container justify="flex-start">
                                                            <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)}>Salvar</Button>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
ExecucaoEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedExecucaoEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(ExecucaoEdit)));
export { connectedExecucaoEditPage as ExecucaoEdit };