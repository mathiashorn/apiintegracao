﻿import { connect } from 'react-redux';
import { execucaoAction } from '../_actions';
import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Nav from '../_components/nav';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100vh',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
});
class Execucao extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(execucaoAction.getExecucao());
    }
    handleChange = event => {
        this.setState({
            anchor: event.target.value,
        });
    };
    handleClick = (event, id) => {
        const { dispatch } = this.props;
        dispatch(execucaoAction.deleteExecucaoById(id));
    };
    render() {

        const { classes } = this.props;
        const { execucao } = this.props.state.execucao;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={24}>
                            <Grid item xs={3} container justify="flex-start">
                                <Typography variant="h2"><b>{'Execuções'}</b></Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                                <Button variant="contained" color="primary" className={classes.button} component='a' href="/execucao">NOVA</Button>
                            </Grid>
                        </Grid>                        
                        <br /><br />
                        <Grid container spacing={24}>
                            <Paper className={classes.root}>
                                <Table className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Data</TableCell>
                                            <TableCell>Rotina/Arquivo</TableCell>
                                            <TableCell>Status</TableCell>
                                            <TableCell>Ações</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            execucao.map(n => {

                                                var descricao = '';
                                                if (n.rotina != null) {
                                                    descricao = n.rotina.descricao;
                                                } else {
                                                    descricao = n.arquivo;
                                                }

                                                var status = ''
                                                if (n.status == null) {
                                                    status = 'Processando';
                                                } else {
                                                    status = n.status;
                                                }

                                                return (
                                                    <TableRow key={n.id}>
                                                        <TableCell component="th" scope="row">
                                                            <Moment format="DD/MM/YYYY">
                                                                {n.dataHoraInicio}
                                                            </Moment>
                                                        </TableCell>
                                                        <TableCell>{descricao}</TableCell>
                                                        <TableCell>{status}</TableCell>
                                                        <TableCell>
                                                            <IconButton className={classes.button} aria-label="Edit" component='a' href={`/execucao/${n.id}`}>
                                                                <EditIcon />
                                                            </IconButton>
                                                            <IconButton className={classes.button} aria-label="Delete" onClick={(event) => this.handleClick(event, n.id)}>
                                                                <DeleteIcon />
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </Paper>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
Execucao.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedExecucaoPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Execucao)));
export { connectedExecucaoPage as Execucao };