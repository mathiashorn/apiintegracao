﻿const initialState = {
    anchor: 'left',
    ajuda: [],
    open: false,
    name: '',
    type: '',
    nullable: false
};

export function ajuda(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_AJUDA':
            return {
                ...state,
                ajuda: action.ajuda
            };
        case 'AJUDA_DETAIL':
            return {
                ...state,
                name: action.name,
                type: action.type,
                nullable: action.nullable
            };
        case "AJUDA_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
