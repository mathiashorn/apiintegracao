﻿const initialState = {
    anchor: 'left',
    rotina: [],
    open: false,
    id: '',
    conexaoId: '',
    descricao: '',
    consulta: '',
    urlDestino: '',
    conexao: []
};

export function rotina(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_ROTINA':
            return {
                ...state,
                rotina: action.rotina
            };
        case 'ROTINA_DETAIL':
            return {
                ...state,
                id: action.id,
                conexaoId: action.conexaoId,
                descricao: action.descricao,
                consulta: action.consulta,
                urlDestino: action.urlDestino,
                conexao: action.conexao,
            };
        case "ROTINA_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
