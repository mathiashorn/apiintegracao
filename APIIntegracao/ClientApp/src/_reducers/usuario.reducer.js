﻿const initialState = {
    anchor: 'left',
    usuario: [],
    open: false,
    id: '',
    email: '',
    nome: '',
    senha: '',
    log: []
};

export function usuario(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_USUARIO':
            return {
                ...state,
                usuario: action.usuario
            };
        case 'USUARIO_DETAIL':
            return {
                ...state,
                id: action.id,
                email: action.email,
                nome: action.nome,
                senha: action.senha,
                log: action.log
            };
        case "USUARIO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
