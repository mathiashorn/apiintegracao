﻿import { combineReducers } from 'redux';

import { authentication } from './auth.reducer';
import { ajuda } from './ajuda.reducer';
import { conexao } from './conexao.reducer';
import { execucao } from './execucao.reducer';
import { parametro } from './parametro.reducer';
import { rotina } from './rotina.reducer';
import { tipoConexao } from './tipoConexao.reducer';
import { usuario } from './usuario.reducer';

const rootReducer = combineReducers({
    authentication,
    ajuda,
    conexao,
    execucao,
    parametro,
    rotina,
    tipoConexao,
    usuario
});

export default rootReducer;