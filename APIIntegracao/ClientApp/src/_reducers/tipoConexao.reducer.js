﻿const initialState = {
    anchor: 'left',
    tipoConexao: [],
    open: false,
    id: '',
    descricao: ''
};

export function tipoConexao(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_TIPOCONEXAO':
            return {
                ...state,
                tipoConexao: action.tipoConexao
            };
        case 'TIPOCONEXAO_DETAIL':
            return {
                ...state,
                id: action.id,
                descricao: action.descricao
            };
        case "TIPOCONEXAO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
