﻿const initialState = {
    anchor: 'left',
    conexao: [],
    open: false,
    id: '',
    tipoConexaoId: '',
    descricao: '',
    connectionString: '',
    tipoConexao: []
};

export function conexao(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_CONEXAO':
            return {
                ...state,
                conexao: action.conexao
            };
        case 'CONEXAO_DETAIL':
            return {
                ...state,
                id: action.id,
                tipoConexaoId: action.tipoConexaoId,
                descricao: action.descricao,
                connectionString: action.connectionString,
                tipoConexao: action.tipoConexao,
            };
        case "CONEXAO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
