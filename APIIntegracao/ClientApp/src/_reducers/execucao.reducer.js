﻿const initialState = {
    anchor: 'left',
    execucao: [],
    open: false,
    id: '',
    tipo: '',
    rotinaId: '',
    arquivo: '',
    dataHoraInicio: '',
    dataHoraTermino: '',
    status: '',
    mensagemRetorno: '',
    execucao: [],
    ArquivoFormFile: null
};

export function execucao(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_EXECUCAO':
            return {
                ...state,
                execucao: action.execucao
            };
        case 'EXECUCAO_DETAIL':
            return {
                ...state,
                id: action.id,
                tipo: action.tipo,
                rotinaId: action.rotinaId,
                arquivo: action.arquivo,
                dataHoraInicio: action.dataHoraInicio,
                dataHoraTermino: action.dataHoraTermino,
                status: action.status,
                mensagemRetorno: action.mensagemRetorno,
                execucao: action.execucao
            };
        case "EXECUCAO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
