import React, { Component } from 'react';
import './App.css';
import { Router, Switch, Route } from 'react-router-dom';
import { Login } from './login/';
import { Home } from './home/';
import { Conexao } from './conexoes/';
import { ConexaoEdit } from './conexoes/';
import { Execucao } from './execucoes/';
import { ExecucaoEdit } from './execucoes/';
import { Parametro } from './parametros/';
import { ParametroEdit } from './parametros/';
import { Rotina } from './rotinas/';
import { RotinaEdit } from './rotinas/';
import { Usuario } from './usuarios/';
import { UsuarioEdit } from './usuarios/';
import { history } from './_helpers';
import { PrivateRoute } from './_components';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router history={history}>
                    <div>
                        <Switch>
                            <PrivateRoute exact path='/home' component={Home} />
                            <PrivateRoute exact path='/conexoes' component={Conexao} />
                            <PrivateRoute exact path='/conexao' component={ConexaoEdit} />
                            <PrivateRoute exact path='/conexao/:id' component={ConexaoEdit} />
                            <PrivateRoute exact path='/execucoes' component={Execucao} />
                            <PrivateRoute exact path='/execucao' component={ExecucaoEdit} />
                            <PrivateRoute exact path='/execucao/:id' component={ExecucaoEdit} />
                            <PrivateRoute exact path='/parametros' component={Parametro} />
                            <PrivateRoute exact path='/parametro' component={ParametroEdit} />
                            <PrivateRoute exact path='/parametro/:id' component={ParametroEdit} />
                            <PrivateRoute exact path='/rotinas' component={Rotina} />
                            <PrivateRoute exact path='/rotina' component={RotinaEdit} />
                            <PrivateRoute exact path='/rotina/:id' component={RotinaEdit} />
                            <PrivateRoute exact path='/usuarios' component={Usuario} />
                            <PrivateRoute exact path='/usuario' component={UsuarioEdit} />
                            <PrivateRoute exact path='/usuario/:id' component={UsuarioEdit} />
                            <Route exact path='/' component={Login} />
                        </Switch>
                    </div>
                </Router>
            </div>
        );
    }
}
export default App;