﻿import { usuarioService } from '../_services/';
import { history } from '../_helpers';

export const usuarioAction = {
    getUsuario,
    getUsuarioById,
    onChangeProps,
    editUsuarioInfo,
    createUsuario,
    deleteUsuarioById
};
function getUsuario() {
    return dispatch => {
        let apiEndpoint = 'usuarios';
        usuarioService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeUsuariosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createUsuario(payload) {
    return dispatch => {
        let apiEndpoint = 'usuarios/';
        usuarioService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/usuarios');
            })
    }
}
function getUsuarioById(id) {
    return dispatch => {
        let apiEndpoint = 'usuarios/' + id;
        usuarioService.get(apiEndpoint)
            .then((response) => {
                dispatch(editUsuariosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editUsuarioInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'usuarios/' + id;
        usuarioService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/usuarios');
            })
    }
}
function deleteUsuarioById(id) {
    return dispatch => {
        let apiEndpoint = 'usuarios/' + id;
        usuarioService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteUsuariosDetails());
                dispatch(usuarioAction.getUsuario());
            })
    };
}
export function changeUsuariosList(usuario) {
    return {
        type: "FETECHED_ALL_USUARIO",
        usuario: usuario
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editUsuariosDetails(usuario) {
    return {
        type: "USUARIO_DETAIL",
        id: usuario._id,
        email: usuario.email,
        nome: usuario.nome,
        senha: usuario.senha,
        log: usuario.log
    }
}
export function updatedUserInfo() {
    return {
        type: "USUARIO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "USUARIO_CREATED_SUCCESSFULLY"
    }
}
export function deleteUsuariosDetails() {
    return {
        type: "DELETED_USUARIO_DETAILS"
    }
}