﻿import { conexaoService } from '../_services/';
import { history } from '../_helpers';

export const conexaoAction = {
    getConexao,
    getConexaoById,
    onChangeProps,
    editConexaoInfo,
    createConexao,
    deleteConexaoById
};
function getConexao() {
    return dispatch => {
        let apiEndpoint = 'conexoes';
        conexaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeConexaosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createConexao(payload) {
    return dispatch => {
        let apiEndpoint = 'conexoes/';
        conexaoService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/conexoes');
            })
    }
}
function getConexaoById(id) {
    return dispatch => {
        let apiEndpoint = 'conexoes/' + id;
        conexaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(editConexaosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editConexaoInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'conexoes/' + id;
        conexaoService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/conexoes');
            })
    }
}
function deleteConexaoById(id) {
    return dispatch => {
        let apiEndpoint = 'conexoes/' + id;
        conexaoService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteConexaosDetails());
                dispatch(conexaoAction.getConexao());
            })
    };
}
export function changeConexaosList(conexao) {
    return {
        type: "FETECHED_ALL_CONEXAO",
        conexao: conexao
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editConexaosDetails(conexao) {
    return {
        type: "CONEXAO_DETAIL",
        id: conexao._id,
        tipoConexaoId: conexao.tipoConexaoId,
        descricao: conexao.descricao,
        connectionString: conexao.connectionString,
        tipoConexao: conexao.tipoConexao,
    }
}
export function updatedUserInfo() {
    return {
        type: "CONEXAO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "CONEXAO_CREATED_SUCCESSFULLY"
    }
}
export function deleteConexaosDetails() {
    return {
        type: "DELETED_CONEXAO_DETAILS"
    }
}