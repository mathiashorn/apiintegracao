﻿import { tipoConexaoService } from '../_services/';
import { history } from '../_helpers';

export const tipoConexaoAction = {
    getTipoConexao,
    getTipoConexaoById,
    onChangeProps,
    editTipoConexaoInfo,
    createTipoConexao,
    deleteTipoConexaoById
};
function getTipoConexao() {
    return dispatch => {
        let apiEndpoint = 'tiposconexoes';
        tipoConexaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeTipoConexaosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createTipoConexao(payload) {
    return dispatch => {
        let apiEndpoint = 'tiposconexoes/';
        tipoConexaoService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/tiposconexoes');
            })
    }
}
function getTipoConexaoById(id) {
    return dispatch => {
        let apiEndpoint = 'tiposconexoes/' + id;
        tipoConexaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(editTipoConexaosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editTipoConexaoInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'tiposconexoes/' + id;
        tipoConexaoService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/tiposconexoes');
            })
    }
}
function deleteTipoConexaoById(id) {
    return dispatch => {
        let apiEndpoint = 'tiposconexoes/' + id;
        tipoConexaoService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteTipoConexaosDetails());
                dispatch(tipoConexaoAction.getTipoConexao());
            })
    };
}
export function changeTipoConexaosList(tipoConexao) {
    return {
        type: "FETECHED_ALL_TIPOCONEXAO",
        tipoConexao: tipoConexao
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editTipoConexaosDetails(tipoConexao) {
    return {
        type: "TIPOCONEXAO_DETAIL",
        id: tipoConexao._id,
        descricao: tipoConexao.descricao
    }
}
export function updatedUserInfo() {
    return {
        type: "TIPOCONEXAO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "TIPOCONEXAO_CREATED_SUCCESSFULLY"
    }
}
export function deleteTipoConexaosDetails() {
    return {
        type: "DELETED_TIPOCONEXAO_DETAILS"
    }
}