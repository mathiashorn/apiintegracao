﻿import { ajudaService } from '../_services/';
import { history } from '../_helpers';

export const ajudaAction = {
    getAjuda,
    onChangeProps
};
function getAjuda(id) {
    return dispatch => {
        let apiEndpoint = 'ajuda/' + id;;
        ajudaService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeAjudasList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
export function changeAjudasList(ajuda) {
    return {
        type: "FETECHED_ALL_AJUDA",
        ajuda: ajuda
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}