﻿import { execucaoService } from '../_services/';
import { history } from '../_helpers';

export const execucaoAction = {
    getExecucao,
    getExecucaoById,
    onChangeProps,
    editExecucaoInfo,
    createExecucao,
    deleteExecucaoById
};
function getExecucao() {
    return dispatch => {
        let apiEndpoint = 'execucoes';
        execucaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeExecucaosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createExecucao(payload) {
    return dispatch => {
        let apiEndpoint = 'execucoes/';
        execucaoService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/execucoes');
            })
    }
}
function getExecucaoById(id) {
    return dispatch => {
        let apiEndpoint = 'execucoes/' + id;
        execucaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(editExecucaosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editExecucaoInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'execucoes/' + id;
        execucaoService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/execucoes');
            })
    }
}
function deleteExecucaoById(id) {
    return dispatch => {
        let apiEndpoint = 'execucoes/' + id;
        execucaoService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteExecucaosDetails());
                dispatch(execucaoAction.getExecucao());
            })
    };
}
export function changeExecucaosList(execucao) {
    return {
        type: "FETECHED_ALL_EXECUCAO",
        execucao: execucao
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editExecucaosDetails(execucao) {
    return {
        type: "EXECUCAO_DETAIL",
        id: execucao._id,
        tipo: execucao.tipo,
        rotinaId: (execucao.rotinaId == null ? '' : execucao.rotinaId),
        arquivo: execucao.arquivo,
        dataHoraInicio: execucao.dataHoraInicio,
        dataHoraTermino: execucao.dataHoraTermino,
        status: execucao.status,
        mensagemRetorno: execucao.mensagemRetorno,
        rotina: execucao.rotina
    }
}
export function updatedUserInfo() {
    return {
        type: "EXECUCAO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "EXECUCAO_CREATED_SUCCESSFULLY"
    }
}
export function deleteExecucaosDetails() {
    return {
        type: "DELETED_EXECUCAO_DETAILS"
    }
}