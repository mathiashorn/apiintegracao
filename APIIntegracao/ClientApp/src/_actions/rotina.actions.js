﻿import { rotinaService } from '../_services/';
import { history } from '../_helpers';

export const rotinaAction = {
    getRotina,
    getRotinaById,
    onChangeProps,
    editRotinaInfo,
    createRotina,
    deleteRotinaById
};
function getRotina() {
    return dispatch => {
        let apiEndpoint = 'rotinas';
        rotinaService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeRotinasList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createRotina(payload) {
    return dispatch => {
        let apiEndpoint = 'rotinas/';
        rotinaService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/rotinas');
            })
    }
}
function getRotinaById(id) {
    return dispatch => {
        let apiEndpoint = 'rotinas/' + id;
        rotinaService.get(apiEndpoint)
            .then((response) => {
                dispatch(editRotinasDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editRotinaInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'rotinas/' + id;
        rotinaService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/rotinas');
            })
    }
}
function deleteRotinaById(id) {
    return dispatch => {
        let apiEndpoint = 'rotinas/' + id;
        rotinaService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteRotinasDetails());
                dispatch(rotinaAction.getRotina());
            })
    };
}
export function changeRotinasList(rotina) {
    return {
        type: "FETECHED_ALL_ROTINA",
        rotina: rotina
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editRotinasDetails(rotina) {
    return {
        type: "ROTINA_DETAIL",
        id: rotina._id,
        conexaoId: rotina.conexaoId,
        descricao: rotina.descricao,
        consulta: rotina.consulta,
        urlDestino: rotina.urlDestino,
        conexao: rotina.conexao
    }
}
export function updatedUserInfo() {
    return {
        type: "ROTINA_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "ROTINA_CREATED_SUCCESSFULLY"
    }
}
export function deleteRotinasDetails() {
    return {
        type: "DELETED_ROTINA_DETAILS"
    }
}