﻿import { parametroService } from '../_services/';
import { history } from '../_helpers';

export const parametroAction = {
    getParametro,
    getParametroById,
    onChangeProps,
    editParametroInfo,
    createParametro,
    deleteParametroById
};
function getParametro() {
    return dispatch => {
        let apiEndpoint = 'parametros';
        parametroService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeParametrosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createParametro(payload) {
    return dispatch => {
        let apiEndpoint = 'parametros/';
        parametroService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/parametros');
            })
    }
}
function getParametroById(id) {
    return dispatch => {
        let apiEndpoint = 'parametros/' + id;
        parametroService.get(apiEndpoint)
            .then((response) => {
                dispatch(editParametrosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editParametroInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'parametros/' + id;
        parametroService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/parametros');
            })
    }
}
function deleteParametroById(id) {
    return dispatch => {
        let apiEndpoint = 'parametros/' + id;
        parametroService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteParametrosDetails());
                dispatch(parametroAction.getParametro());
            })
    };
}
export function changeParametrosList(parametro) {
    return {
        type: "FETECHED_ALL_PARAMETRO",
        parametro: parametro
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editParametrosDetails(parametro) {
    return {
        type: "PARAMETRO_DETAIL",
        id: parametro._id,
        chave: parametro.chave,
        valor: parametro.valor,
        descricao: parametro.descricao
    }
}
export function updatedUserInfo() {
    return {
        type: "PARAMETRO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "PARAMETRO_CREATED_SUCCESSFULLY"
    }
}
export function deleteParametrosDetails() {
    return {
        type: "DELETED_PARAMETRO_DETAILS"
    }
}