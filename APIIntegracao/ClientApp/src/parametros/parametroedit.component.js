﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Nav from '../_components/nav';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { parametroAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100vh',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
});
class ParametroEdit extends Component {
    
    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(parametroAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;       

        if (params.id) {
            const { dispatch } = this.props;
            dispatch(parametroAction.getParametroById(params.id));
        }
    }

    handleClick(event) {

        const { match: { params } } = this.props;
        const { dispatch } = this.props;

        let payload = {
            id: params.id,
            chave: this.props.parametro.chave,
            valor: this.props.parametro.valor,
            descricao: this.props.parametro.descricao
        }

        if (params.id) {
            dispatch(parametroAction.editParametroInfo(params.id, payload));
        } else {
            dispatch(parametroAction.createParametro(payload));
        }
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;
        function InsertText(props) {
            return <Typography variant="h2"><b>{'Novo Parâmetro'}</b></Typography>;
        }

        function EditText(props) {
            return <Typography variant="h2"><b>{'Editar Parâmetro'}</b></Typography>;
        }
        function SegHeader() {
            if (params.id) {
                return <EditText />;
            }
            return <InsertText />;
        }
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={24}>
                            <Grid item container xs={3}>
                                <SegHeader />
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <form className={classes.container}>
                                            <Grid container spacing={24}>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="chave"
                                                            label="Chave"
                                                            className={classes.textField}
                                                            value={this.props.parametro.chave}
                                                            onChange={this.handleChange('chave')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>                                                    
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valor"
                                                            label="Valor"
                                                            className={classes.textField}
                                                            value={this.props.parametro.valor}
                                                            onChange={this.handleChange('valor')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>    
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="descricao"
                                                            label="Descrição"
                                                            className={classes.textField}
                                                            value={this.props.parametro.descricao}
                                                            onChange={this.handleChange('descricao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>       
                                            </Grid>
                                            <br />
                                            <Grid container spacing={24}>
                                                <Grid item xs={3}>
                                                </Grid>
                                                <Grid item xs={6}>
                                                </Grid>
                                                <Grid item xs={3} container justify="center">
                                                    <Grid container spacing={24}>
                                                        <Grid item xs={6} container justify="center">
                                                            <Button variant="contained" color="secondary" className={classes.button} component='a' href="/parametros">Cancelar</Button>
                                                        </Grid>
                                                        <Grid item xs={6} container justify="flex-start">
                                                            <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)}>Salvar</Button>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
ParametroEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedParametroEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(ParametroEdit)));
export { connectedParametroEditPage as ParametroEdit };