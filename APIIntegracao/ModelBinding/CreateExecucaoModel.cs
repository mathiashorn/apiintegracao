﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIIntegracao.ModelBinding
{
    [ModelBinder(typeof(JsonWithFilesFormDataModelBinder), Name = "json")]
    public class CreateExecucaoModel
    {
        public int? Id { get; set; }
        [Required]
        public char Tipo { get; set; }
        public int? RotinaId { get; set; }
        public string Arquivo { get; set; }
        //[Required]
        public IFormFile ArquivoFormFile { set; get; }
        public DateTime? DataHoraInicio { get; set; }
        public DateTime? DataHoraTermino { get; set; }
        public string Status { get; set; }
        public string MensagemRetorno { get; set; }
    }
}
